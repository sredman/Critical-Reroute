#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json
import random

from netdiff import NetJsonParser

class TopologyFailer:

    def __init__(self, filename: str):
        """
        Load the specified file
        """
        self.test_filename = filename
        self.parser = NetJsonParser(file=filename, directed=True)

    def fail(self, percent: int) -> str:
        nodes = self.parser.graph.nodes

        failures = []
        for node in nodes:
            if random.random() * 100 < percent:
                failures.append(node)

        for node in failures:
            self.parser.graph.remove_node(node)

        return self.parser.json()



if __name__ == '__main__':
    parser = argparse.ArgumentParser("Fail some nodes of the given topologoy and write a new topology file to stdout")
    parser.add_argument("--input-filename", action='store', type=str, required=True,
                        help="Path to the result file to read")
    parser.add_argument("--percent-failures", action='store', type=int, required=True,
                        help="Expected percentage of nodes to remove from the input graph")

    args = parser.parse_args()

    failer = TopologyFailer(args.input_filename)

    print(failer.fail(args.percent_failures))