\documentclass[letterpaper,twocolumn,12pt]{article}
\usepackage{amsmath}
\usepackage{authblk}
\usepackage{caption}
\usepackage{cleveref}
\usepackage{epsfig}
\usepackage{fancyhdr} % Allow custom headers
\usepackage[margin=1in,left=1.5in]{geometry}
\usepackage{indentfirst} % Indent first sentence of paragraph
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{newfloat}
\usepackage{paralist}
\edef\restoreparindent{\parindent=\the\parindent\relax}
\usepackage{parskip}
\restoreparindent
\usepackage{placeins}
\usepackage[doublespacing]{setspace} % Double-spaced
\usepackage{subcaption}
\usepackage{textcomp} % Copyright, etc. symbols
\usepackage{tgtermes} % Set default font
\usepackage{titling}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{url}
\usepackage{wrapfig}
\usepackage{xspace}

\title{Critical Segment Routing}
\author{Simon Redman}
\affil{School of Computing, University of Utah}
\date{27 April 2018}

\pdfinfo{
  /Author (Simon Redman)
  /Title  (Critical Segment Routing)
  /Date   (27 April 2018)
}

\newenvironment{allintypewriter}{\ttfamily}{\par}

\DeclareFloatingEnvironment[fileext=frm,placement={!ht},name=Equation]{equationfloat}
\captionsetup[equationfloat]{labelfont=bf}

\begin{document}


\onecolumn
\pagenumbering{gobble} % Suppress page numbers for the first page
\begin{center}
  \textsc{\large Critical Reroute}\\
  by

  \theauthor

  \vspace{1.5cm}
  \begin{singlespace}
    A Senior Honors Thesis Submitted to the Faculty of\\
    The University of Utah\\
    In Partial Fulfillment of the Requirement for the
  \end{singlespace}

  Honors Degree in Bachelor of Science

  In

  Computer Science
\end{center}

\vfill % Bottom-align everything which follows
\noindent
Approved:
\vspace{1cm}
\setlength{\columnsep}{2.5cm}
\begin{multicols}{2}
  \hrule
  \noindent
  Jacobus van der Merwe, PhD\\*
  Thesis Faculty Supervisor

  \vspace{2cm}
  \hrule
  \noindent
  Erin Parker, PhD\\*
  Honors Faculty Advisor

  \vfill
  \columnbreak

  \hrule
  \noindent
  Ross Whitaker, PhD\\*
  Chair, School of Computing

  \vspace{2cm}
  \hrule
  \noindent
  Sylvia D. Torti, PhD\\*
  Dean, Honors College

  \vfill
\end{multicols}
\setlength\columnsep{12pt}

\begin{center}
  \begin{singlespace}
    April 2018\\*
    Copyright \textcopyright \space 2018\\*
    All Rights Reserved
  \end{singlespace}
\end{center}

\pagebreak

\addcontentsline{toc}{section}{Abstract}
\section*{Abstract}
Due to network failure, underprovisioning, or temporary unanticipated increases in traffic, networks may become overloaded. In such situations, it may be desirable to prioritize some traffic flows over others, such as for public safety applications or priority customers. Solutions such as priority queuing and differentiated services provide partial answers to that goal, but leave other problems unsolved. The work presents a novel solution using Segment Routing and a Genetic Algorithm optimizer to minimize the impact of network overload on critical traffic flows. The results show that these methods are able to reroute flows using a single midpoint such that the total network overload is less than traditional shortest-path routing while avoiding unnecessarily long paths and taking priority of flows into account.
\vfill
\pagenumbering{roman}
\setcounter{page}{2}
\pagebreak

\begin{singlespace}
  \tableofcontents
  \listoffigures
\end{singlespace}
\pagebreak

\onecolumn
\pagestyle{fancy} % Use a fancy header/footer
\fancyhf{}  % Clear everything from the footer
\renewcommand{\headrulewidth}{0pt} % Do not show a horizontal rule in the header
\fancyhead[R]{\thepage} % Right-align the page number in the header
\pagenumbering{arabic}

\section{Introduction}
It is rare for any network failure to happen in isolation. In a disaster situation, at the same time as paths are failing due to the disaster, increased traffic from the general public might overwhelm any remaining paths, leaving emergency services crippled and unable to make effective use of the network.

Clearly, in such a situation, fast restoration of routes is key. In addition, some control over which end-to-end routes are chosen for a given class of traffic would be ideal in order to ensure priority traffic, such as that belonging to emergency services or to a priority customer, isn't overwhelmed.

A traditional solution to this situation might involve priority queuing. However, while priority queues might address the goals of critical flows, they do not answer the basic problem of overload for non-critical flows, leaving the packet loss and delay unsolved.

This work leverages the basic idea presented in ``Optimizing Restoration with Segment Routing'' \cite{HaoOptimizingrestorationsegment2016} of using cost function optimization to determine segment routing midpoints, but removes the central assumptions that a network has sufficient bandwidth to avoid overload in case of link failure, and that a flow can be infinitely divided. Instead, this work assume that a link failure will result in overload which needs to be addressed and that end-to-end flows are discretely divisible.

The key insight to this research is that, in a particular overloaded network, there are likely to be some links which are overloaded and some which are not. Leveraging the bandwidth available on neighboring links to reduce overload on another link should generally improve the performance of the network, and doing so in a way which allows some flows to be prioritized above others ensures that priority services continue to function unimpeded. However, this must be achievable in an automated way, since it would be impossible to a network operator to react quickly enough to a disaster situation to keep the network running smoothly.

% Maybe worth saying something about the hardness of this problem? Somewhere in the realm of knapsack?

The remainder of the paper is structured as follows: First, a presentation of background topics needed to understand the research, then a high level discussion of the design of Critical Segment Routing, followed by a description of its implementation. Finally, the results are presented and analyzed.

\textit{Assumptions:}
It is assumed that, for a given segment-routed network, there exists a traffic matrix for every flow, and that the flows in this matrix can be assigned some criticality which is usable for making routing decisions.

\textit{Contributions:}
This work shows the viability of using a genetic algorithm to select midpoints for a segment-routed network, and uses this algorithm to assign paths based on administrator-defined criticality weightings.

\section{Background}

\subsection{Segment Routing}
Segment Routing \cite{FilsfilsSegmentRoutingArchitecture2015} provides the tools necessary to describe end-to-end routes with sufficient detail to be useful, while leveraging the efficiency and fast error recovery of local routing decisions. This gives more control over a network flow than simple end-to-end shortest-path routing, while avoiding the fragility of explicit-path routing.

In brief, the idea of segment routing is to store a linked list of midpoint segments in the packet header, where segments might be a particular node, a particular link of a particular node, or a particular service offered by a particular node. The packet is then routed from the source to the first segment, and from there to the next, and so on. Routing between segments is handled using traditional routing strategies, while still allowing a network operator enough detail for significant traffic engineering.

Since traffic is routed segment-to-segment using traditional routing strategies, in case of a link failure, rerouting in a segment-routed network works identically to a traditional network, including leveraging any fast-reroute strategies which might already exist in the network, and the control plane will use its existing method of finding a new least-cost path between each midpoint. Of course, as with any routing strategy, if a specified segment is unavailable the network will not be able to serve any flows which rely on that segment, meaning some method of quickly handling rerouting at the controller level is required.

\subsection{Genetic Algorithm}
A Genetic Algorithm \cite{DarrellGeneticAlgorithmTutorial1994} is a method of cost-function minimization based on the theory of biological evolution.

To start, a number of `genetic' representations are generated, either at random or based on some heuristic, representing a proposed solution. Then, at every step, the genomes are tested against the cost function to determine their `fitness', with the fittest solutions being given the best chance to be selected to reproduce. A new generation is created by choosing two representations and selecting partial solutions from each of them, with a chance of some random mutation for each solution component. The process is then repeated until a desired fitness is reached or a certain number of iterations has been run. Finally, the best solution in the gene pool is returned.

Genetic Algorithms have the rare advantage among minimization algorithms of working on non-smooth cost functions. Since the process of assigning $n$ midpoints turns out to be an $n$-dimensional step function, which is a necessary feature for the cost function used in this work.

Like other optimizers, there are several parameters which change the behavior of a genetic algorithm such as mutation chance, crossover chance, and gene pool size. Selecting good parameters is important to ensure successful use of genetic algorithms, but selecting the right parameters for a particular problem is a matter of experience and experimentation.

\subsection{Priority Queuing}
A traditional approach to traffic prioritization might use priority queuing, for instance by using the IP-level DSCP \cite{NicholsDefinitionDifferentiatedServices} service bits. However, while this approach might resolve the problem of prioritizing traffic, it does not address the key issue of network overload, meaning non-priority flows suffer significantly from delay and packet loss. The presented solution of rerouting traffic to reduce overload of a particular link efficiently avoids these issues.


\subsection{Related Work}

\textbf{Optimizing Restoration with Segment Routing \cite{HaoOptimizingrestorationsegment2016} :}
Presents a Fully Polynomial Time Approximation Scheme for planning restoration routes for a segment-routed network, and spawned the base idea for this research. However, where their work assumes there will be sufficient bandwidth in the network to make up for any link failure, this research extend this model to cover cases where link failure will lead to congestion which needs to be managed.

Additionally, their work assumes a given flow can be arbitrarily broken up. This has the advantage of making their cost function continuous and doubly-differentiable, which may not be realistic, but which is required for many traditional cost function minimization algorithms. The presented methods do not make that requirement of a network, allowing an entire flow to be routed through a single midpoint.

% Dear Simon: If you want to cite this, read the paper first
%\textbf{Optimal Routing in ad-hoc Network Using Genetic Algorithm} \cite{mohammed2012optimal} Earlier work in applying a genetic algorithm to generating routes, but lacks the Segment Routing and Criticality factors

\textbf{Optimizing Optimizing Segment Routing using Evolutionary Computation \cite{PereiraOptimizingSegmentRouting2017} :}
Presents research using segment routing to achieve traffic engineering goals, in particular of distributing traffic volume across a network by using a genetic algorithm to set link weights and a probabilistic model to assign segment routing rules. This work differs in that it applies a genetic algorithm directly to the problem of assigning segment routing midpoints and adds the consideration of prioritizing critical traffic flows.

\section{Design}
\label{sec:design}

\subsection{System Architecture}
\begin{figure}[h]
  \includegraphics[width=\columnwidth]{"Figures/2018-03-28 Architecture Overview".png}
  \caption[Architecture Overview]{Diagram illustrating how the discussed route planner fits into a network}
  \label{fig:ArchitectureOverview}
\end{figure}

\Cref{fig:ArchitectureOverview} shows how the route planner which is the result of this research fits into the larger context of a software-defined network. The controller communicates with the network to learn the current topology and traffic density and sends that information to the route planner. The route planner calculates suggested midpoints for the network, as is discussed in \Cref{sec:implementation}, and provides those midpoints to the controller, which then converts the midpoints into routing rules which are pushed to the network.

\subsection{Cost Function}
The most obvious incarnation of a cost function is to, for a given list of flows, look at every link in the network and examine how much bandwidth is required compared with how much is available, and take the summation of the overload over every link in the network. In other words
{\normalsize \begin{align*}
& Cost(e) =
  \begin{cases} 
    R(e) - C(e) & R(e) > C(e) \\
    0           & \text{otherwise} 
  \end{cases}\\ \  
& Total\ Cost = \sum_{e} Cost(e)
\end{align*}}
\captionof{equationfloat}{Unweighted cost function}

Where $e$ is a link, $R(e)$ is the requested bandwidth on that link, and $C(e)$ is the capacity of the link.

For na\"ive routing this cost function is sufficient, but it does not take into account the criticality of any flows. To account for that, the overload at every link is multiplied by the criticality of the most critical flow on that link, resulting in
{\normalsize \begin{align*}
&Cost(e) =
\begin{cases} 
(R(e) - C(e)) * crit(e) & R(e) > C(e) \\
0           & \text{otherwise} 
\end{cases}\\ \  
&Total\ Cost = \sum_{e} Cost(e)
\end{align*}}
\captionof{equationfloat}{Criticality-weighted cost function}

Where $e$, $R(e)$, and $C(e)$ are as above, and $crit(e)$ is the criticality weight of the most critical flow being routed on a given link. The value of that weight can be any number greater than zero, with values less than one indicating a low-priority flow and values greater than one indicating a high-priority flow. The criticality does not correlate to any real-world constant or measurement, but is simply a relative scale of how important one flow is compared with another, so it can take any value greater than zero which the network operator decides to assign to a flow.

As part of the evaluation both versions of the cost function are tested to see if the criticality weighting performs as expected.

When this research was beginning, it was assumed that the overload cost alone would be sufficient to encourage the optimizer to select not only routes which would not overload the network, but also routes which would tend to avoid unnecessary length to avoid wasting network bandwidth. However, since any link which is not overloaded does not contribute to the cost function and since, in the tested topology the available bandwidth on any link is relatively large compared to a single flow, the optimizer can generate long paths using low-utilization links which nevertheless do not overload the network.

To combat this, a second term representing the number of extra links required for a pure Open Shortest Path First (OSPF) route and the number of links used in the segment-routed path is added to the genetic optimizer as a secondary target. This means that the optimizer will try to optimize cost first, then within the optimized cost will try to find the shortest path.

This new term, $stretch$, is defined as\\*
{\normalsize
\begin{equation*}
Stretch(k) = |SR\ Path(k)| - |OSPF\ Path(k)|
\end{equation*}
\begin{flalign*}
Total\ Stretch = \sum_{k} (Stretch(k))
\end{flalign*}
}
\captionof{equationfloat}{Stretch function}
Where $k$ is a particular flow

\subsection{Minimization Algorithm}
Though it may not be readily apparent, the cost functions above, while benefiting from simplicity, have the significant problem of being non-continuous, thus non-differentiable. The discontinuity is caused because changing a single flow's midpoint is a discrete operation, and results in a discrete change in the cost function. This results in an $n$-dimensional step function, where $n$ is the number of flows.

This causes a problem, since the majority of common minimization algorithms (for example, Gradient Descent) required a smooth cost function. There are a limited handful of minimization algorithms which do not require a twice-differentiable function. One such is a Genetic Algorithm. Though there are a few other options, a Genetic Algorithm was chosen for this research for its relative simplicity.

\subsection{Gene Pool}
In the context of this problem, a single genome is a vector of midpoint nodes, with one entry per flow representing the single segment-routed midpoint for the flow. The number of genomes in the gene pool is one of the parameters to the genetic algorithm.

For this research, the planner assigns one midpoint per flow. As discussed in \cite{HaoOptimizingrestorationsegment2016}, one midpoint gives good control while minimizing algorithm complexity. If more midpoints were desired, these methods could easily be extended to include these in the gene pool by making each genome a two-dimensional matrix, where each column represents the list of midpoints for the particular flow.

\section{Implementation}
\label{sec:implementation}

\subsection{Genetic Algorithm Initial Seeding}
In the current implementation, the entire gene pool is initialized with genomes which are created from the shortest-path midpoints. Initalizing the algorithm with known-reasonable midpoints has the advantage of converging much more quickly and avoiding path stretch, but means that the optimizer might start in a local minimum which it has no chance to escape. Experimenting with totally random initial solution assignments resulted in the algorithm taking a prohibitively long time to converge and did not always result in better solutions. As an avenue for future work, some middle ground should be found.

\subsection{Genetic Algorithm Parameters}
As mentioned in the background section, selecting reasonable parameters for a genetic algorithm is quite difficult. A too-large mutation chance might result in the algorithm performing similarly to random search, while a too-small mutation chance might take a prohibitively long time to converge. The parameters used for the evaluation of this work were selected by human intuition, followed by tweaking to get a reasonable result. These parameters are likely not optimal, and future work involves finding better ones.

\subsection{Genetic Algorithm Termination}
The genetic algorithm is run for a fixed number of iterations, which is one of the parameters passed to the current implementation. After it runs the specified amount of time, the best result is returned. However, in some cases the algorithm converged much more quickly than others, and occasionally it failed to converge in the number of iterations allowed. Future work should involve improving the algorithm to detect when it has converged and terminate early, and either report that it failed to terminate after reaching the iteration limit or continue running until convergence.

\section{Evaluation and Analysis}

\subsection{Experiment Setup}
\begin{figure}[t]
  \centering
  \includegraphics[width=\columnwidth/2]{"Figures/2018-03-28 Experiment Topology".png}
  \caption[Experiment Topology]{Visual representation of the experiment topology}
  \label{fig:ExperimentTopology}
\end{figure}
The topology used to test this implementation is a traditional star network with four core switches and two distribution layers with a fanout of 3 at each layer. As shown in \Cref{fig:ExperimentTopology}, this means the network has a total of 52 switches, 36 of which are edge switches. Each switch at the core is connected to every other switch, making a total of 54 bidirectional links in the network. Each link is assumed to have the same bandwidth, and OSPF routing weights are assigned as the inverse of the bandwidth on a link.

Since this topology is so simple, it occasionally happens that OSPF and the genetic optimizer converge to the same overload cost. This suggests that experiments on more complicated (non-star) topologies would yield more interesting data.

Similarly, assigning bandwidth based on the link's depth in the star might make the topology more realistic, and contribute to reducing the path stretch in the routes suggested by the genetic optimizer.

Using this topology, traffic matrices were generated by selecting source and destination pairs from the set of edge routers, then assigning a specific portion of those to be 'high' demand, a portion to be 'medium' demand, and the rest to be 'low' demand. A criticality of 5 was assigned to an equal portion of every bandwidth demand group. The load on the network is then scaled up and down by generating more or fewer traffic pairs.

With each traffic matrix, data was gathered by measuring the overload factor using plain OSPF, then by running the genetic minimizer five times and selecting the best result, to try to compensate for the algorithm converging to a local minimum, then running the genetic algorithm another five times, but ignoring the criticality factor in the cost function, to test whether the criticality weighting was performing as expected.

Every result was evaluated using the cost function including criticality weighting, the difference in number of links used compared to OSPF, and the total number of critical paths routed on overloaded links.

\subsection{Analysis}

\begin{table*}[t]
  \centering
  \begin{tabular}{ | c || c | c | c | c |}
    \hline
    Method   & worse than OSPF & equal OSPF & beat OSPF & Average path stretch \\ \hline
    na\"ive  & 0               & 68         & 412       & 0.0113 links \\ \hline
    weighted & 0               & 60         & 420       & 0.0120 links \\ \hline
  \end{tabular}
  \caption{Summary of the results of the genetic algorithm-generated midpoint routing}
  \label{tab:ospf-compare}
  
  \vspace*{5 mm}
  
  \begin{tabular}{ | r | c |}
    \hline
    weighted worse than na\"ive & 155 \\ \hline
    weighted equal na\"ive      & 127 \\ \hline
    weighted beat na\"ive       & 198 \\ \hline
  \end{tabular}
  \caption{Comparison of the genetic algorithm methods}
  \label{tab:genetic-compare}
\end{table*}

Tables \ref{tab:ospf-compare} and \ref{tab:genetic-compare} provides a high-level overview comparing the performance of OSPF, the na\"ive cost function which ignores the criticality of flows when trying to select optimal midpoints, and the weighted cost function which takes the criticality factor into account. Regardless of how the path was determined, the shown cost was calculated using the criticality-weighted cost function.

The data in Table \ref{tab:ospf-compare} shows some promising results. First, it is worth mentioning that neither of the genetic optimizers ever finds a path worse than OSPF. Since the optimizer is seeded with OSPF midpoints, it is a good sign that neither ever does worse than OSPF since that would indicate the algorithm were diverging. Instead, we see that in about 85\% of cases both genetic optimizers find a lower-cost solution than OSPF. Of the remaining cases where the algorithms came up with the same solution as OSPF, in about half (32) of the experiments both genetic optimizers and OSPF found the same-cost path. However, this means that in the remaining trials where one optimizer was unable to find any optimization beyond the initially-seeded OSPF midpoints, the other function found a better solution. This suggests that initializing the gene pool to OSPF midpoints might be causing the algorithm to get stuck in that local minimum from which it is sometimes difficult to escape.

Table \ref{tab:genetic-compare} also suggests that the algorithm suffers from local optima in at least some cases. From the shown data, we see that in about one third of cases the na\"ive optimizer found a lower-cost solution, despite the final result being scored using the weighted cost function. Absent local minima, it would be expected that either the na\"ive or weighted solution would be consistently better than the other, especially considering that the na\"ive solution is optimized for a slightly different cost function.

\begin{figure}[t]
  \includegraphics[width=\columnwidth]{"Figures/2018-04-09 Naive_vs_Criticality".png}
  \caption[Na\"ive routing vs. criticalitiy-conscious routing]{Comparison of na\"ive midpoint routing costs and criticality-conscious midpoint routing costs. Negative indicates na\"ive has a lower cost.}
  \label{fig:NaiveVsCriticality}
\end{figure}

\begin{figure*}[t]
  \begin{subfigure}[t]{0.45\textwidth}
    \includegraphics[width=\columnwidth]{"Figures/2018-04-09 OSPF_vs_Criticality".png}
    \caption[OSPF costs vs. criticality-conscious midpoint routing costs]{Comparison of OSPF costs and criticality-conscious midpoint routing costs. Negative indicates OSPF has a lower cost.}
    \label{fig:OSPFvsCriticality}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.45\textwidth}
    \includegraphics[width=\columnwidth]{"Figures/2018-04-09 OSPF_vs_Naive".png}
    \caption[OSPF costs vs. na\"ive midpoint routing costs]{Comparison of OSPF costs and na\"ive midpoint routing costs. Negative indicates OSPF has a lower cost.}
    \label{fig:OSPFvsNaive}
  \end{subfigure}
  \caption{Path Stretch}
\end{figure*}

\Cref{fig:NaiveVsCriticality}, \Cref{fig:OSPFvsCriticality}, and \Cref{fig:OSPFvsNaive} show the cost function data in more detail. In \Cref{fig:OSPFvsCriticality} and \Cref{fig:OSPFvsNaive}, the result of the cost function for running the specified genetic algorithm on a particular flow matrix is compared to the cost for the same flow matrix generated by OSPF. \Cref{fig:NaiveVsCriticality} is similar, but compares the two genetic algorithms directly.

Recalling that, for a given datapoint, all tests were run on the same traffic matrix, \Cref{fig:NaiveVsCriticality} is interesting, as it again suggests that the cost function suffers significantly from local minima. Since this figure compares the two different kinds of runs of the genetic algorithm, it would make sense if, in general, both algorithms converged to a similar value or if one algorithm consistently beat out the other. However, Table \ref{tab:genetic-compare} shows that the algorithms come up with the same value in about a quarter of cases, and from \Cref{fig:NaiveVsCriticality} we can see that the cases where they differ can differ significantly in either direction. This suggests that, in many cases, the one optimizer was getting stuck in some local minimum which the other algorithm managed to avoid.

Taken together, \Cref{fig:OSPFvsCriticality} and \Cref{fig:OSPFvsNaive} show how the final cost function evaluation compares to the cost of the OSPF route, where a larger difference means the OSPF route was more expensive. Of note is that the points appears to be distributed in bands, with many runs of the experiment resulting in exactly the same value. This is likely due to an artifact of how the traffic matrices were generated for these experiments. Since these matricies were generated with three tiers of bandwidth requests and each tier always requested the same amount of bandwidth, any trials with different traffic matricies would be able to affect the cost function at the same granularity, thus having a good chance of changing the cost function by the same amount. Again noteworthy in comparing these two graphs is there does not seem to be significant consistency between the two optimizers. For instance, at the peak value in \Cref{fig:OSPFvsNaive} where the difference between the na\"ive solver and the OSPF solution was 11250, the corresponding difference from the weighted solver was only 3250. This lack of consistency between high and low values in the two solvers suggests that the optimizer needs more research to behave more consistently.

\begin{figure*}[h]
  \begin{subfigure}[t]{0.45\textwidth}
    \includegraphics[width=\columnwidth]{"Figures/2018-04-09 Criticality_Stretch_Per_Flow".png}
    \caption[Criticality-conscious average path stretch]{The average number of extra links used by the weighted genetic algorithm per flow as compared to OSPF paths}
    \label{fig:CriticalityStretch}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.45\textwidth}
    \includegraphics[width=\columnwidth]{"Figures/2018-04-09 Naive_Stretch_Per_Flow".png}
    \caption[Na\"ive average path stretch]{The average number of extra links used by the na\"ive genetic algorithm per flow as compared to OSPF paths}
    \label{fig:NaiveStretch}
  \end{subfigure}
  \caption{Path Stretch}
\end{figure*}

\Cref{fig:CriticalityStretch} and \Cref{fig:NaiveStretch} show the number of extra links needed per flow for the two different runs of the genetic algorithm as compared to OSPF. We can see from Table \ref{tab:ospf-compare} that the average of these tables is nearly the same and that the value is very small. This is a promising result, since it means the optimizer is deviating from the OSPF route occasionally. However, since the value is so small, only about one additional link per hundred flows, it opens the question of whether the optimizer could do better if it deviated further from the OSPF path, and whether seeding the gene pool with OSPF paths has significantly harmed the optimizer's ability to explore for new solutions.

In spite of the remaining research problems, the presented results indicate that the basic idea of using a genetic optimizer to assign segment routing midpoints is sound and is worth pursuing as a solution to a difficult problem.

\section{Future Work}

This research was originally motivated by the goal of pre-emptively planing for network failure and the resulting overload by pre-computing routes given a possible failure, as was done in \cite{HaoOptimizingrestorationsegment2016}. The problem is simplified by assuming the network begins overloaded and calculating routes to resolve the situation. Even though the optimizer runs fairly quickly, it may be unacceptable to leave the network in an overloaded state for even a short time. Future work should return to the real-world use-case of preparing for a disaster rather than reacting to it.

% There is probably room for some words about local optima, in particular since once a midpoint is assigned it would be unlikely to be unassigned, so any path stretch it contributes would be hard to get rid of even if there is a better solution

\textbf{Genetic Algorithm Optimization:}
\begin{compactitem}
  \item The minimization parameters of the genetic algorithm are not currently very rigorously selected, and it is likely that further exploration of the parameter space would yield significant gains.
  \item The current algorithm tries to minimize path stretch as a secondary consideration after minimizing the cost function. This means there is no tradeoff between a path which is exceptionally long but low-cost compared to a path which is significantly shorter and slightly higher cost.
  \item Since the entire gene pool is allowed to reproduce with any other member, most every member in the gene pool will converge to the same genome. This might be resolved by adding some kind of island strategy \cite{DarrellGeneticAlgorithmTutorial1994}, such that multiple genetically-diverse solutions of similar optimality can exist simultaneously
  \item Genetic Algorithms benefit significantly when the solution structure can be exploited. If multiple alleles which appear near each other in the solution are somehow related, allowing the recombination operation to keep such groups together is essential. For example, perhaps flows which originate or terminate at the same node affect other such flows. The presented solution has not yet been explored for such structure.
\end{compactitem}

\textbf{Simulation Optimizations:}
\begin{compactitem}
  \item The current simulation assumes all links have the same capacities. Future work should involve a more interesting topology with realistic bandwidths.
  \item The current topology is not very exciting, meaning OSPF often comes up with the best path. Future work should involve a more varied topology.
\end{compactitem}

\section{Conclusion}
This work has presented Critical Segment Routing, which makes use of Segment Routing and a Genetic Algorithm to allow edge-to-edge traffic shaping based on administrator-assigned criticality weighting. Evaluation of the presented solution shows that, while the current implementation appears to function reasonably well, future research has the potential to unlock significant gains in performance and functionality.

\addcontentsline{toc}{section}{Acknowledgements}
\section*{Acknowledgments}
I would like to thank my advisor, Professor van der Merwe, for his broad knowledge and advice, as well as his willingness to let me define my own goals and fit them to a project. Thanks also to Binh Nguyen, without whom the research questions in this project would have never been thought of. Also, big thanks to David Johnson for answering the seemingly unending stream of technical questions.

Thanks to the Flux Research Group for supporting this work. This work is supported in part by the National Science Foundation under grant number 1647264.

\vfill
\pagebreak % End Acknowledgements page

\onecolumn
\singlespacing
\nocite{FortinDEAPEvolutionaryAlgorithms2012}
\nocite{HagbergExploringNetworkStructure2008}
\addcontentsline{toc}{section}{References}
{
  %\footnotesize 
  \small 
  \bibliographystyle{acm}
  \bibliography{./biblio}
}
\vfill
\pagebreak % End References page

\vspace*{\fill}

\begin{center}
  \begin{tabular}{ l l }
    Name of Candidate: & Simon Redman              \\
                       &                           \\
    Birth Date         & January 18, 1995          \\
                       &                           \\
    Birth Place:       & Los Alamos, New Mexico    \\
                       &                           \\
    Address:           & 1450 S Madera Hills Drive \\
                       & Bountiful, Utah 84010
  \end{tabular}
\end{center}
\vspace*{\fill}
\end{document}



