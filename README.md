## How to setup

Since this is a python project, setup is relatively straightforward using virtualenv and pip

First, make sure you have Python's virtualenv installed. It is packaged as `python-virtualenv` on Ubuntu, CentOS (by default) and RHEL (with EPEL enabled), and `python2-virtualenv` on Fedora

With virtualenv installed, run:

> virtualenv env

To create a virtual environment in the env folder, then run:

> source env/bin/activate

To enable the virtual environment. Be sure the environment is active before finishing the setup or before running!

Next, we use the virtual environment's `pip` to install the dependencies for this project

> pip install -r requirements.txt

Wait for this command to finish. Setup is complete!

## How to run the demo

- Stand up topology "ammon-topology-simplified" with name cr-demo

- On CB
  - Start nlsdn:
    - `sudo mkdir /var/lib/nlsdn`
    - `git clone https://gitlab.flux.utah.edu/safeedge/nlsdn`
    - `pushd nlsdn`
    - `virtualenv -p /usr/bin/python2 env`
    - `source env/bin/activate`
    - `pip install git+https://gitlab.flux.utah.edu/safeedge/pyroute2@safeedge`
    - `./setup.py install`
    - Follow repo instructions for setup and configuration, found here: https://gitlab.flux.utah.edu/safeedge/nlsdn
    - Run using `sudo $(which nlsdn-flask) -f ./config.json` (using the correct path to your config file)
  - Clone demo container:
    - Follow instructions in https://gitlab.flux.utah.edu/safeedge/demo-capsule
  - Launch one or more OSPF sniffer
    - More than one sniffer in a connected region can make the controller unhappy, so I suggest only launching one
    - Note that the sniffer should be run before the orchestrator. If the sniffer needs to
    be restarted, make sure to re-run ospf on all nodes (e.g., re-run the orchestrator)
    else there will be nothing to sniff!
    - `pushd demo-capsule/ospfv3_monitor`
    - `virtualenv -p /usr/bin/python2 env`
    - `source env/bin/activate`
    - `pip install -r requirements.txt`
    - `sudo $(which python) main.py --controller=CB --port=8080 --log-file=/tmp/sniffer.log & echo $! > /tmp/sniffer.pid && disown; deactivate`
    - `deactivate`
  - Run orchestrator:
    - The orchestrator is designed such that it will simply set the state to the desired state.
    It *should* therefore be safe to run multiple times. HOWEVER, there is currently a bug
    in the handling of launching rwflowpack and friends, so those need to be manually killed
    before re-running the orchestrator.
        - That could be easil accomplished by simply rebooting the server being used as the controller (for instance, CB)
        - However, you could also run these commands:
            - `sudo ip6tables -D INPUT -j NETFLOW && sudo ip6tables -D OUTPUT -j NETFLOW && sudo ip6tables -D FORWARD -j NETFLOW # Delete iptables rules for collecting netflow`
            - `sudo modprobe -r ipt_NETFLOW`
            - At this point, it may also be desireable to delete rwflowpack's cached data:
              - `rm -rf /data/{in,int2int,ext2ext,out}`
    - Change to demo-capsule/segment-routing-orchestrator
    - Set up new python3 environment: `virtualenv -p /usr/bin/python3 env`
    - Enter environment `source env/bin/activate`
    - Set up environment `pip install -r requirements.txt`
    - `./orchestrator.py --netgraph-write="/tmp/netinfo.json" --controller-name="CB" --border-regex="CB" --no-sniffer`
  - Start SDN controller:
    - cd demo-capsule/segment-routing-sdn-controller
    - `virtualenv -p /usr/bin/python3 env`
    - `source env/bin/activate`
    - `pip install git+https://gitlab.flux.utah.edu/safeedge/pyroute2@safeedge`
    - `pushd ~/nlsdn/; ./setup.py install; popd`
    - `pip install -r requirements.txt`
    - Start controller: `./debug_ryu.py --config-file=params.conf`
      - Note that the params.conf file specifies the location of the netinf.json
      file and how to identify which nodes are running the NLSDN app
      - If you change either of those things, be sure to update params.conf!
  - Launch webapp
   - `cd demo-capsule/segment-routing-webapp`
   - `./replace_sdn_controller_name.sh <CB's public IP address>`
   - Start the webapp: python3 -m http.server
   - Point your browser to <CB's public IP address>:8000/web-portal and you should see a view of the topology!
  - Set up DemoRunner
    - `git clone https://gitlab.flux.utah.edu/sredman/Critical-Reroute.git`
    - `cd Critical-Reroute`
    - Set up traffic sniffer
      - `virtualenv -p /usr/bin/python2 env2`
      - `source env2/bin/activate`
      - Download netsa Python library:
        - `wget https://tools.netsa.cert.org/releases/netsa-python-1.5.tar.gz`
        - `tar -xvf netsa-python-1.5.tar.gz`
        - `pushd netsa-python-1.5`
        - `./setup.py install`
        - `popd`
      - `pip install -r requirements2.txt`
      - `deactivate`
  - Run demo
    - `cd Critical-Reroute`
    - `virtualenv -p /usr/bin/python3 env3`
    - `source env3/bin/activate`
    - `pip install -r requirements.txt`
      - As of this writing, requirements.txt relies on a not-released version of netdiff.
      If you get an error about that package, install the latest version by doing `
      `pip install git+https://github.com/openwisp/netdiff@master` and then try installing requirements.txt again.
    - `./DemoRunner.py --controller=cb.cr-demo.safeedge.emulab.net`
    - Add some flow rules using the demo UI, then generate some traffic (using iperf3 or so)

## File Listing

As you have probably noticed, this repository contains a *lot* of Python files.
Sorry about that.

Most of them are not interesting, and are either quickly slapped-together scripts for
doing something I needed to do which will probably never need to be run again.

Most everything in the repository is an executable which can be run
with `--help`` to at least get a one-sentence description of what the file does.
Due to the fast-moving nature of this project, some executable libraries are broken
and only work in library form. (Some libraries are not used at all, so are extra broken)

The following is a list of what I consider to be the most useful files.
Any files which are not on this list I consider "not interesting".

  1. `CriticalReroute.py`
    - This file is the core library which contains the actual genetic optimizer implementation
    - It is used by nearly every other file on this list. Look to those for usage guides.
  2. `TrafficMatrixGenerator.py`
    - These days, this is less of a "Generator" and more of a library, although it does
    also contain the code for generating random traffic matrices
    - This contains the data structure definition for TrafficMatrix object.
  3. `netsa-TrafficMatrix-reader.py`
    - This script is to be run on the node which is running the SiLK netsa collector
    to export traffic matricies from the live-collected data.
    - This is basically standalone from the rest of the project, and is only housed
    here because this is the place where traffic matrices are consumed.
    - If you would like to view (relatively unfiltered) traffic information from the command line,
    run `rwfilter --proto=0-255 --type=all --pass=stdout | rwuniq --fields=sIP,dIP,sPort,dPort,sTime,eTime --values=bytes`
  4. `DemoRunner.py`
    - This is probably the file you were looking for
    - Run Critical Reroute interactively on a live topology using live data
  5. `DataGatherer.py`
    - For a lot of uses, this can be described as the "main executable" of the project
    - This collects a definition of an experiment to run, then orchestrates a CriticalReroute
    object and collects the data from it. It writes the results of the experiment to stdout
    (so if it's important to you, I suggest piping stdout to a file to save the result...)
    - Once upon a time it uses randomly-generated inputs, now it can only
    use saved test files. There is no reason it has to be this way, but
    I have no reason to change it to use the randomly-generated stuff.
    Such is the price of progress.
    - Exported test files can be munched in a variety of ways:
      1. `DataCleaner.py`
        - This incredibly simple script just converts the output test file into .csv format
        for use in making figures, etc.
      2. `TraceAnalyzer.py`
        - Basically the same job as `DataCleaner.py`, but collects different columns into
        a different-shaped .csv for different graphs.
      3. `ResultFileCombiner.py`
        - Very hastily combine several result files into one so that it can be converted
        to a single .csv by the above scripts
        - This is likely to be very error-prone, but it works for simple cases
      4. `ResultToTestConverter.py`
        - Convert the output of a test run into inputs for a new test run!
        - The idea is to be able to simulate running the optimizer with a new traffic matrix
        or topology using a partial (previous) midpoint assignment
      5. `TopologyFailer.py`
        - Delete some nodes from the passed topology and write a new topology to stdout
        - Useful as part of simulating a failure situation
        - Very fragile. I ended up just running this script multiple times to until I
        got a non-broken topology out.
