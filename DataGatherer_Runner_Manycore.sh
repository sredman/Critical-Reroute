#!/usr/bin/env bash

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

NUM_CORES=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')

function doit {

  for file in $(ls ${TESTDIR}*.cr-test.json); do
    fname=$(basename $file)
    mkdir ${OUTDIR}/${fname}
      for (( core=1; core <= ${NUM_CORES}; core++ )) do
        ./DataGatherer_Runner.sh ${file} ${core} "${OUTDIR}/${fname}/" &
      done

      for (( core=1; core <= ${NUM_CORES}; core++ )) do
        wait
      done
  done
}

num_args=$#

if [[ num_args -ne 2 ]]; then
    echo "Illegal number of arguments"
    echo "Usage: $0 <testdir> <prefix>"
    echo "Where:"
    echo "<testdir> is the path to a directory containing .cr-test.json files to run"
    echo "<outdir> is some folder to put results"
    exit 1
fi

TESTDIR=$1
OUTDIR="$2"

doit
