#!/usr/bin/env python

# Copyright (C) 2017 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import random

from CriticalReroute import CriticalReroute
from StarTopoGenerator import StarTopoGenerator
from TrafficMatrixGenerator import TrafficMatrixGenerator


class CriticalRerouteRandomSample(CriticalReroute):
    """
    An implementation of Critical Reroute which, instead of using any kind of informed optimizer,
    randomly guesses midpoints and returns the best one it finds
    """

    def calculate_midpoint(self,
                           traffic_matrix,
                           num_samples):
        """
        Randomly sample for the best midpoint assignment to minimize the cost function

        :param traffic_matrix:
        :param num_samples: Number of random guesses to make
        :return: A list of the best-found midpoint assignments
        """

        best_midpoints = []
        best_cost = float('inf')
        all_routers = self.graph.getAllRouters()

        for sample in range(num_samples):

            if self.verbose and sample % (num_samples / 10) == 0:
                # Print out the best-found cost every so often, to tell if the selected parameters
                # tend to trend downwards
                print "Iteration: " + str(sample) + " Cost: " + str(best_cost)

            curr_midpoints = []
            for i in range(len(traffic_matrix)):
                curr_midpoints.append(random.choice(all_routers))

            curr_cost, curr_stretch, unused = self.cost_function(curr_midpoints, traffic_matrix, ignore_criticality=False)

            if curr_cost < best_cost:
                best_cost = curr_cost
                best_midpoints = curr_midpoints

        return best_midpoints, best_cost


if __name__ == "__main__":
    """
    Test code to exercise this file
    """
    num_core_routers = StarTopoGenerator.NUM_CORE_ROUTERS
    num_lvl1_routers = StarTopoGenerator.NUM_LVL1_FANOUT
    num_lvl2_routers = StarTopoGenerator.NUM_LVL2_FANOUT

    graph, edge_routers = StarTopoGenerator.generate(core_routers=num_core_routers,
                                       lvl1_routers=num_lvl1_routers,
                                       lvl2_routers=num_lvl2_routers)

    # LOG.basicConfig(format='%(levelname)s:%(message)s', level=LOG.INFO)

    all_links = graph.getAllLinks()

    link_capacities = {}
    for link in all_links:
        # For now, assign all links the same capacity
        link_capacities[link] = CriticalReroute.DEFAULT_LINK_CAPACITY

    ospf_weights = {}
    # Assign ospf weights as the inverse of the link capacity
    for link in all_links:
        ospf_weights[link] = 1.0 / link_capacities[link]

    traffic_matrix, total_bandwidth = TrafficMatrixGenerator.RandomTieredTrafficMatrix(
        edge_routers,
        num_entries=150
    )

    random_router = CriticalRerouteRandomSample(graph, link_capacities, ospf_weights)

    best_midpoints, best_cost = random_router.calculate_midpoint(traffic_matrix,
                                                                 num_samples=2000*125)

    print traffic_matrix

    for flow_index in range(0, len(traffic_matrix)):
        print "Src: " + str(traffic_matrix[flow_index].source_id)\
              + " Dst: " + str(traffic_matrix[flow_index].dest_id)\
              + " Mid: " + str(best_midpoints[flow_index])\
              + " Criticality: " + str(traffic_matrix[flow_index].criticality)

    print "Total Cost: " + str(best_cost)
    print "Total Bandwidth Requested: " + str(total_bandwidth)

    ospf_midpoints = [flow.source_id for flow in traffic_matrix]
    ospf_cost = random_router.cost_function(ospf_midpoints,
                                            traffic_matrix,
                                            ignore_criticality=False)

    print "Plain OSPF routing cost, including criticality"
    print str(ospf_cost)
    print ""