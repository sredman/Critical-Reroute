#!/usr/bin/python2

# Copyright (C) 2017 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import random
from collections import namedtuple, deque
from operator import attrgetter

TrafficMatrixEntry = namedtuple('TrafficMatrixEntry', ['source_id', 'dest_id', 'bandwidth_req', 'criticality', 'sport', 'dport', ])
TrafficMatrixEntry.__new__.__defaults__ = (None, None, )


class TrafficMatrix(list):
    """
    A traffic matrix is simply a vector of TrafficMatrixEntries
    """
    pass

    def sort(self, column: str, reverse=False):
        """
        Sort the traffic matrix based on the requested column. Accepts the same arguments as list.sort(..)

        :param column: Column of TrafficMatrixEntry to sort on. Must be a member of TrafficMatrixEntry._fields
        :param reverse: Passing reverse will sort the list in decending order
        :return: None (Same as list.sort())
        """
        if column not in TrafficMatrixEntry._fields:
            raise ValueError("Sort column must be a column of TrafficMatrixEntry")

        return super(TrafficMatrix, self).sort(key=attrgetter(column), reverse=reverse)

    def distribute(self, column: str):
        """
        "Sort" the matrix such that the values of the requested column are distributed to avoid
        same value entries being next to each other.

        For example, if the original array is [1, 2, 4, 2, 3, 2, 1, 4], the distributed array might be
        [1, 4, 3, 2, 1, 4, 2, 2]

        Notice that, though the list is not actually sorted, after we see a particular '2' we don't see
        another until we have seen one of every other remaining value

        Operates stably on the list

        :param column: Column of the list to distribute on
        :return: None
        """

        if column not in TrafficMatrixEntry._fields:
            raise ValueError("Column must be a column of TrafficMatrixEntry")

        buckets = []  # We will use one bucket per different thing we find in the matrix column

        for entry in self:
            target_value = getattr(entry, column)

            # Find the bucket which already has columns of this same value
            # This implementation could be optimized, but I assume that the number of buckets will
            # be small (<< 100) and that this method is only going to run a few times
            bucketed = False  # Keep track of whether we were able to find a matching bucket
            for bucket in buckets:
                if getattr(bucket[0], column) == target_value:
                    bucket.append(entry)
                    bucketed = True

            if not bucketed:
                bucket = deque([entry])
                buckets.append(bucket)
            del bucketed

        # Now that we have bucketed all of the same values, time to distribute them
        index = 0  # Point to the next index in the matrix to overwrite
        bucket_idx = 0  # Point to the next bucket to read an element from
        while index < len(self):
            bucket_idx += 1
            bucket_idx %= len(buckets)

            bucket = buckets[bucket_idx]

            entry = bucket.popleft()
            self[index] = entry
            index += 1

            if len(bucket) == 0:
                # If the bucket is empty remove it from the list and update the pointer
                buckets.pop(bucket_idx)
                bucket_idx -= 1


class TrafficMatrixGenerator:
    """This class describes various traffic matrix generators
    """

    @staticmethod
    def RandomTieredTrafficMatrix(edge_routers,
                                  num_entries=100,
                                  percentage_high_bandwidth=0.2,
                                  percentage_medium_bandwidth=0.5,
                                  percentage_low_bandwidth=0.3,
                                  high_bandwidth=750,
                                  medium_bandwidth=500,
                                  low_bandwidth=250,
                                  criticality_percentage=0.2,
                                  critical_criticality=5):
        """Generate a traffic matrix with random source and destination endpoints

        All traffic is divided into three 'tiers', some high-bandwidth traffic, some
        medium-bandwidth traffic, and some low-bandwidth traffic

        :param edge_routers: List of valid routers for source or destination
        :param num_entries: Number of entires to generate
        :param percentage_high_bandwidth: What percentage of entries should have high bandwidth
        :param percentage_medium_bandwidth: What percentage of entries should have medium bandwidth
        :param percentage_low_bandwidth: What percentage of entries should have low bandwidth
        :param high_bandwidth: What value should be assigned to high bandwidth
        :param medium_bandwidth: What value should be assigned to medium bandwidth
        :param low_bandwidth: What value should be assigned to low bandwidth
        :param criticality_percentage: What percentage of each tier of flow should be critical
        :param critical_criticality: What criticality weight should be assigned to critical flows
        :return: A traffic matrix and the total amount of bandwidth requested
        """

        assert percentage_high_bandwidth + percentage_medium_bandwidth + percentage_low_bandwidth == 1.0

        total_bandwidth = 0
        traffic_matrix = TrafficMatrix()

        total_high_bandwidth = num_entries * percentage_high_bandwidth
        total_medium_bandwidth = num_entries * percentage_medium_bandwidth
        total_low_bandwidth = num_entries * percentage_low_bandwidth

        high_bandwidth_start_index = 0
        high_bandwidth_end_index = high_bandwidth_start_index + total_high_bandwidth

        medium_bandwidth_start_index = high_bandwidth_end_index
        medium_bandwidth_end_index = medium_bandwidth_start_index + total_medium_bandwidth

        low_bandwidth_start_index = medium_bandwidth_end_index
        low_bandwidth_end_index = low_bandwidth_start_index + total_low_bandwidth

        for index in range(0, num_entries):
            if index < high_bandwidth_end_index:
                # Generate some high-bandwidth requests
                bandwidth_req = high_bandwidth
            elif index > total_high_bandwidth and index < total_medium_bandwidth:
                # Generate some medium-bandwidth requests
                bandwidth_req = medium_bandwidth
            else:
                if index < total_low_bandwidth * criticality_percentage + total_high_bandwidth + total_medium_bandwidth:
                    bandwidth_req = low_bandwidth

            if random.random() < criticality_percentage:
                criticality = critical_criticality
            else:
                criticality = 1

            total_bandwidth += bandwidth_req
            source_id = random.choice(edge_routers)
            dest_id = random.choice(edge_routers)
            traffic_matrix.append(TrafficMatrixEntry(source_id=source_id,
                                                     dest_id=dest_id,
                                                     bandwidth_req=bandwidth_req,
                                                     criticality=criticality))
        random.shuffle(traffic_matrix)
        return traffic_matrix, total_bandwidth