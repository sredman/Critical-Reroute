#!/usr/bin/env bash

# Copyright (C) 2017 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

NUM_RUNS=10 # Number of tests to run
NUM_REPEATS=10 # Number of times a particular test will be run
TEST_RUNTIME=180000

HOSTNAME=$(hostname)

FILENAME_PREFIX="CriticalRerouteData-"
FILENAME_SUFFIX=".json"

function doit {
  for (( run=1; run <= ${NUM_RUNS}; run++ )) do
    filename="${FILENAME_PREFIX}${HOSTNAME}-${FILENAME_DIFFERENTIATOR}-${run}${FILENAME_SUFFIX}"
    ./DataGatherer.py --filename=${INPUT_FILENAME} --runtime-milliseconds=${TEST_RUNTIME} --num-repeats=${NUM_REPEATS} --skip-unweighted=true > ${filename}
  done
}

num_args=$#

if [[ num_args -ne 3 ]]; then
    echo "Illegal number of arguments"
    echo "Usage: $0 <filename> <differentiator> <prefix>"
    echo "Where:"
    echo "<filename> is the path to a .cr-test.json file to run"
    echo "<differentiator> is some unique string to differentiate the output filename of this data run so other data doesn't get overwritten"
    echo "<prefix> is some filename prefix"
    exit 1
fi

INPUT_FILENAME=$1
FILENAME_DIFFERENTIATOR=$2
FILENAME_PREFIX="$3${FILENAME_PREFIX}"

source env3/bin/activate
doit
