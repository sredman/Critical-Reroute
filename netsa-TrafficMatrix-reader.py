#!/usr/bin/env python

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

try:
    from netsa import script, util
except ImportError as e:
    print(e)
    print("Unable to import netsa module. Maybe you need to download it (https://tools.netsa.cert.org/netsa-python/index.html)")
    exit(-1)

from collections import namedtuple
from datetime import datetime, timedelta
import ipaddress
import json
from netdiff import NetJsonParser
import pytz

from TrafficMatrixEntryEncoder import TrafficMatrixEntryEncoder

# Can't import Python 3 TrafficMatrixEntry because I used Python 3 syntax in that file. Whoopsies.
TrafficMatrixEntry = namedtuple("TrafficMatrixEntry", ['source_ip', 'dest_ip', 'sPort', 'dPort', 'bandwidth_req', 'criticality', 'start_time',])
DataLine = namedtuple("DataLine", ['sIP', 'dIP', 'sPort', 'dPort', 'proto', 'sTime', 'eTime', 'bytes'])

# Combine all flows which share these fields, regardless of their other field values
CombiningFields = namedtuple("CombiningFields", ['sIP', 'dIP', 'dPort'])

script.set_title("CriticalReroute Traffic Matrix Parser")
script.set_description("""
This script builds a Traffic Matrix suitable for consumption by Critical Reroute
from traffic flow information gathered by the netsa framework
""")
script.set_version("1.0")
script.set_authors([
    "Simon Redman <sredman@cs.utah.edu>",
])

script.add_int_param(
    "time-window",
    required=True,
    help="Historical time window (in seconds) to use for traffic volume aggregation"
)

# By default, netsa blocks clobbering of existing files. To override, set the environment variable SILK_CLOBBER")
# Sorry that this had to be a comment, but I can't see a way to override the netsa library's error handling
script.add_output_file_param(
    "output-file",
    help="Where to export the gathered data as a json-arry formatting of a TrafficMatrix",
    mime_type="text/json"
)

# I'm not actually sure what this does, but it is related to later calling script.get_flow_params()
# It does seem to give us some fancy command-line input switches, though whether those are desirable
# is an open question. Use "--type=all" to get everything (I think)
script.add_flow_params(require_pull=True)


def main():
    outfile = script.get_output_file("output-file")
    timewindow = script.get_param("time-window")

    flow_params = script.get_flow_params()
    sensors = flow_params.get_sensors()
    if sensors is None:
        raise script.UserError("Unable to enumerate sensors")

    # Bin based on a standard 5-tuple
    binning_fields = [
        "sIP",
        "dIP",
        "sPort",
        "dPort",
        "proto",
        "sTime",
        "eTime",
    ]

    # The value we actually care about is the traffic volume (per time unit) associated with each 5-tuple
    aggregation_fields = [
        "bytes",
    ]

    all_filtered_data = {}

    for sensor in flow_params.by_sensor():
        endtime = pytz.utc.localize(datetime.utcnow())
        starttime = endtime - timedelta(seconds=timewindow)

        result = util.shell.run_collect(
            ["rwfilter --proto=0-255 --sensors=%(sensors)s --type=all --pass=stdout",
            "rwuniq --fields=%(binning)s --values=%(aggregation)s",],
            vars={
                'sensors': str(sensor._sensors[0]),
                'binning': str.join(',', binning_fields),
                'aggregation': str.join(',', aggregation_fields),
                # I can't get the 'start-date' and 'end-date' parameters to rwfilter working, so just going to filter in-code
            },
        )
        # The headers are the first line of the output (and should be in the same order as binning_fields + aggregation_fields)
        headers = filter(lambda s: len(s) > 0, map(lambda s: s.strip(), str.splitlines(result[0])[0].split("|")))
        unparsed_data = str.splitlines(result[0])[1:]

        parsed_data = []
        # Super lamely parse the incoming data into the DataLine format
        for line in unparsed_data:
            # As with the headers, the input data needs to be split on '|' (and whitespace cleaned)
            # Maybe I should have used a regex. Oh well.
            fields = filter(lambda s: len(s) > 0, map(lambda s: s.strip(), str.splitlines(line)[0].split("|")))
            # Pair fields with their keys
            paired_fields = {key: value for key, value in zip(binning_fields + aggregation_fields, fields) }

            # Convert to proper Python types
            typed_fields = {}
            # Construct a list of tuples of keys to convert paired with their conversion function
            # The conversion function is responsible for making the value the right type as well
            # as make sure it is sane
            converters = [
                (['sTime', 'eTime',], lambda time: pytz.utc.localize(datetime.strptime(time, "%Y/%m/%dT%H:%M:%S"))),
                (['sPort', 'dPort', 'proto'], lambda value: int(value)),
                (['sIP', 'dIP'], lambda ip: ipaddress.ip_address(u"%s" % str(ip))),
            ]
            for keys, converter in converters:
                for key in keys:
                    typed_fields[key] = converter(paired_fields[key])

            for key in ('bytes',):
                total_traffic = int(paired_fields[key])
                duration = typed_fields['eTime'] - typed_fields['sTime']
                bandwidth = total_traffic/duration.seconds if duration.seconds > 0 else total_traffic
                typed_fields[key] = bandwidth

            assert set(typed_fields.keys()) == set(paired_fields.keys()), "Some keys were not handled by the type conversion"
            parsed_data.append(DataLine(**typed_fields))

        # Get rid of lines which fall outside of our time range
        # Accept all lines which either start within the window or were still running in the window time
        # (I think) This is a little inaccurate if a large flow runs for a long time, but this is the best I can do
        # with the current bucketing from netsa's tools
        filtered_data = []
        for line in parsed_data:
            if line.eTime > starttime:
                filtered_data.append(line)

        # Add to all_filtered_data, combining on CombiningFields if necessary
        for line in filtered_data:
            key = CombiningFields(
                sIP=line.sIP,
                dIP=line.dIP,
                dPort=line.dPort,
            )
            if key not in all_filtered_data:
                all_filtered_data[key] = line
                continue

            other_line = all_filtered_data[key]

            # Take whichever flow ended most recently
            if other_line.eTime > line.eTime:
                pass # Leave other_line in the dictionary
            elif line.eTime > other_line.eTime:
                all_filtered_data[key] = line
            else:
                # Both ended at the same time? Okay.. Take average of bandwidths?
                fields = line._asdict()
                fields['bytes'] = (other_line.bytes + line.bytes) // 2
                new_line = DataLine(**fields)
                all_filtered_data[key] = new_line

    traffic_matrix = []
    # Translate the source and destination IP addresses of data which remains to the labels
    # used by the rest of the tools and convert them to TrafficMatrixEntries
    for line in all_filtered_data.values():
        source_ip = line.sIP
        dest_ip = line.dIP

        traffic_matrix.append(TrafficMatrixEntry(source_ip=source_ip, dest_ip=dest_ip,
                                                     sPort=line.sPort, dPort=line.dPort,
                                                     bandwidth_req=line.bytes, criticality=None,
                                                     start_time=line.sTime,
                                                     ))

    # Write to output as json
    json_traffic_matrix = []
    for line in traffic_matrix:
        json_traffic_matrix.append(line._asdict())

    encoded_json = json.dumps(json_traffic_matrix, cls=TrafficMatrixEntryEncoder)
    outfile.write(encoded_json)

script.execute(main)