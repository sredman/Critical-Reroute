#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json

from TestFileLoader import TestFileLoader
from CriticalReroute import Cost

class ResultConverter:
    """
    Load a test case as described by the specification in TEST-FILE-FORMAT.md
    """

    def __init__(self, filename: str):
        """
        Load the test file specified
        """
        self.test_filename = filename

        with open(filename, 'r') as input:
            self.data = json.load(input)

    def convert_to_test_file(self) -> str:
        out_data = {}

        best_midpoints = max(zip([Cost(**cost) for cost in self.data['weighted-costs']], self.data['weighted-midpoints']))[1]

        original_test_loader = TestFileLoader(self.data['filename'])

        out_data['version'] = '20190325'
        out_data['graph-type'] = 'file' # Support generated tests later
        out_data['graph-filename'] = original_test_loader.graph_filename
        out_data['traffic-matrix'] = self.data['traffic-matrix']
        out_data['initial-midpoints'] = best_midpoints

        return json.dumps(out_data)



if __name__ == '__main__':
    parser = argparse.ArgumentParser("Convert the outputs of a test run from DataGatherer.py to a new test file")
    parser.add_argument("--input-filename", action='store', type=str, required=True,
                        help="Path to the result file to read")
    parser.epilog = "Writes result to stdout"

    args = parser.parse_args()

    converter = ResultConverter(args.input_filename)

    print(converter.convert_to_test_file())