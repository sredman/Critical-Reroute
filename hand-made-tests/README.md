This folder contains a few hand-crafted tests to demonstrate specific capabilities of Critical Reroute

### mini-ammon
This test demonstrates Critical Reroute's ability to spread traffic across multiple links.
There are three flows originating from the CB going to N10, each which is under the link bandwidth but any two of which would result in overload.
The OSPF weighting means that the default path is CB -> N2 -> N10, so that path is badly overloaded.
If run for about 50 iterations (30 milliseconds), Critical Reroute decides to route one flow via N7 and another via N8, thus solving the overload problem.

### mini-ammon-broken
This test demonstrates Critical Reroute's abulity to protect critical traffic flow over others.
The setup is the same as `mini-ammon`, except that the link CB <-> N8 has been removed.
There are now three flows, any two of which could overload a single path, but only two available paths.
One flow is marked as having priority 10, the others have priority 1
If run for about 50 iterations (30 milliseconds), Critical Reroute decides to either put both low-priority flows on the non-default route or to reroute the priority flow.