#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import ipaddress
import json
import netdiff
import networkx
from PyInquirer import prompt
import requests
import subprocess
from collections import namedtuple

from typing import Dict, List, Set, Tuple

from CriticalReroute import CriticalReroute, IndividualDiff
from TrafficMatrixGenerator import TrafficMatrix, TrafficMatrixEntry

class FlowRule:
    """
    Define the matches and the priority value for flows in order to assign priorities to the traffic matrix
    """

    def __init__(self,
                 src: ipaddress.IPv6Network,
                 dst: ipaddress.IPv6Network,
                 src_port: int,
                 dst_port: int,
                 priority: int,
                 ):
        self.src = src
        self.dst = dst
        self.src_port = src_port
        self.dst_port = dst_port
        self.priority = priority

    def __str__(self):
        parts = []
        parts.append(f"prio: {self.priority}")
        parts.append(f"src {self.src}")
        parts.append(f"dst {self.dst}")

        if self.src_port:
            parts.append(f"src-port: {self.src_port}")
        if self.dst_port:
            parts.append(f"dst-port: {self.dst_port}")

        return str.join(", ", parts)

    def __eq__(self, other):
        my_keys = vars(self)
        other_keys = vars(other)

        if not my_keys == other_keys:
            return False

        for key in my_keys:
            if not getattr(self, key) == getattr(other, key):
                return False
        return True

    def __lt__(self, other) -> bool:
        if self.src.prefixlen > other.src.prefixlen:
            return True
        if other.src.prefixlen > self.src.prefixlen:
            return False
        if self.dst.prefixlen > other.dst.prefixlen:
            return True
        if other.dst.prefixlen > self.dst.prefixlen:
            return False

        if self.src_port is not None and other.src_port is None:
            return True
        if self.src_port is None and other.src_port is not None:
            return False

        if self.dst_port is not None and other.dst_port is None:
            return True
        if self.dst_port is None and other.dst_port is not None:
            return False

        return True

    def __hash__(self):
        return (
                       hash(self.src) * (0 if self.src_port is None else self.src_port + 1) +
                       hash(self.dst) * (0 if self.dst_port is None else self.dst_port + 1)
               ) * (self.priority + 1)

"""
A PushableFlow is a match/action pair we will push to the network to define the midpoint of some traffic
matching the source/dest pair
This is not much like a FlowRule, in spite of the naming similarity
"""
PushableFlowRule = namedtuple("PushableFlowRule", ['dpid', 'midpoint', 'src', 'dst', 'sport', 'dport',])

class DemoRunner:

    INSERT_FLOW_ENDPOINT = "flow_mgmt/insert"
    DELETE_ALL_FLOWS_ENDPOINT = "flow_mgmt/delete_all_flows"
    GET_TOPOLOGY_ENDPOINT = "ospf_monitor/get_topology_netjson"
    NLSDN_IDS_ENDPOINT = "nlsdn-mapping"

    def __init__(
            self,
            default_bandwidth: int,
            management_host: str,
            management_port: int = 8080,
            debug_topology: str = None,
            debug_traffic: str = None,
    ):
        self.flow_rules: Set[FlowRule] = set()
        self.current_midpoints: List[int] = None

        self.management_host = management_host
        self.management_port = management_port

        self.default_bandwidth = default_bandwidth

        self.debug_topology = None
        self.debug_traffic = debug_traffic
        if debug_topology:
            self.debug_topology = netdiff.NetJsonParser(file=debug_topology)

        self.controller_url_template = f"http://{self.management_host}:{self.management_port}/" + "{endpoint}"
        pass

    @staticmethod
    def validate_ip_address(ip):
        try:
            ipaddress.IPv6Network(ip)
            return True
        except Exception as e:
            return str(e)

    @staticmethod
    def validate_int(input):
        if input is "" or input is None:
            return True

        try:
            int(input)
            return True
        except Exception as e:
            return str(e)

    def _get_all_dpids(self) -> Dict[str, int]:
        """
        Get a mapping of management ips to dpids
        :return:
        """
        dpids_url = self.controller_url_template.format(endpoint=self.NLSDN_IDS_ENDPOINT)
        dpids_response = requests.get(dpids_url)
        dpids_response.raise_for_status()

        return json.loads(dpids_response.content)

    def _remove_all_paths(self):
        """
        Actually do the work of removing all flow rules from the network
        """

        # Get the list of available router IDs
        ip_to_dpids = self._get_all_dpids()

        dpids = [dpid for dpid in ip_to_dpids.values()]

        # Send the request to delete all flow rules for all routers
        delete_url = self.controller_url_template.format(endpoint=self.DELETE_ALL_FLOWS_ENDPOINT)
        body = {"dpids": dpids}
        delete_response = requests.post(delete_url, json=body)
        delete_response.raise_for_status()

        # Delete from local cache too
        self.current_midpoints = None

    def _find_shared_subnet_addr(self, topology: networkx.Graph, node1, node2) -> ipaddress.IPv6Address:
        """
        Returns the address of node2 which is on the interface which shares a subnet with node1
        If there are multiple such interfaces, return the address of an arbitrary one
        return None if there are no such interfaces
        """
        src_ifaces = topology.nodes[node1]['interfaces']
        dst_ifaces = topology.nodes[node2]['interfaces']

        for src_ip in src_ifaces.values():
            src_net = ipaddress.IPv6Network(src_ip, strict=False)
            for dst_ip_str in dst_ifaces.values():
                dst_ip = ipaddress.IPv6Interface(dst_ip_str).ip
                if dst_ip in src_net:
                    return dst_ip
        return None

    def lookup_label_by_ip(netgraph: networkx.Graph, ip: ipaddress.IPv6Address):
        """
        Get the label in the graph of the node which is assigned this ip address, or None if no such node exists

        This isn't going to be particularly graceful; just look at every node until we find the one we want

        :param netgraph: networkx.Graph to search
        :param ip: IP address to find
        :return:
        """

        matches = []
        for node in netgraph._node:
            interfaces = netgraph._node[node]['interfaces']
            for interface in interfaces:
                address = interfaces[interface].split("/")[0]
                address = ipaddress.ip_address(address)
                if address == ip:
                    matches.append(node)
        if len(matches) == 0:
            return None
        elif len(matches) == 1:
            return matches.pop()
        else:
            raise Exception("More than one interface with IP {ip} was found: {nodes}".format(ip=str(ip),
                                                                                             nodes=str.join(", ",
                                                                                                            matches)))

    def lookup_address_for_node(self, topology: networkx.Graph, src_node, dst_node) -> ipaddress.IPv6Address:
        """
        This needs to find the IP address of the requested node which is "facing" the source of the traffic
        being sent via it. "Facing", in the sense that it's the interface which is most closely facing
        the source.
        In case of two interfaces of equal "facing-ness", pick an arbritary one
        """

        # Part 1 - Check if the soutce and dest nodes share a subnet
        shared_subnet_ip = self._find_shared_subnet_addr(topology, src_node, dst_node)
        if shared_subnet_ip is not None:
            return shared_subnet_ip

        # Part 2
        # We know the nodes are not on the same subnet, so we need to figure out which next-hop of the DESTINATION
        # is "closest" to the source node (in terms of OSPF), call this node cst_node
        # Then we need to figure out which subnet cst_node and dst_node share
        # Then we need to return the IP address of the interface of dst_noode which is on the same subnet as cst_node
        neighbors = topology.adj[dst_node]

        cst_node = None
        cst_dist = float('inf')

        for neighbor in neighbors:
            distance = len(networkx.single_source_dijkstra_path(topology, src_node)[neighbor])
            if distance < cst_dist:
                cst_dist = distance
                cst_node = neighbor

        if cst_node is None:
            raise RouteNotFoundError(f"Could not find a node which shares a subnet with {dst_node}")

        shared_subnet_ip = self._find_shared_subnet_addr(topology, cst_node, dst_node)
        if shared_subnet_ip is not None:
            return shared_subnet_ip
        else:
            raise RouteNotFoundError(f"Unable to find a path between {src_node} and {dst_node}")

    def _install_single_path(self, rule: PushableFlowRule) -> None:
        url = self.controller_url_template.format(endpoint=self.INSERT_FLOW_ENDPOINT)
        data = {
            "dpid": rule.dpid,
            "match": {
                "ipv6_src": str(rule.src),
                "ipv6_dst": str(rule.dst),
            },
            "actions": {
                "srv6_dst": [str(rule.midpoint),],
            }
        }
        # For the demo, ignore source port since the traffic is being aggregated over a large timespan
        # and the source port often changes
        # if rule.sport:
        #     data['match']['sport'] = rule.sport
        if rule.dport:
            data['match']['dport'] = rule.dport

        response = requests.post(url, json=data)
        response.raise_for_status()

    def delete_flow_rules(self, to_delete: Set[FlowRule]):
        self.flow_rules -= to_delete

    def confirm_delete_flow_rules(self, to_delete: List[str]):
        if len(to_delete) == 0:
            return True

        message = str.join("\n", to_delete)
        message += "\n Will be deleted. Are you sure?"

        question = [{
            'type': 'confirm',
            'name': 'sure',
            'message': message,
            'default': False,
        }]

        return prompt(question)['sure']

    def list_flow_rules(self, already_checked: Set[FlowRule] = None):
        if already_checked is None:
            already_checked = set()
        options = []
        for flow in self.flow_rules:
            options.append({
                'name': str(flow),
                'value': flow,
                'checked': flow in already_checked,
            })

        flows = [{
            'type': 'checkbox',
            'name': 'flows',
            'message': 'Select a flow to delete. Leave blank to go back without deleting',
            'choices': options,
        }]

        to_delete = set(prompt(flows)['flows'])

        confirm = self.confirm_delete_flow_rules(list(map(str, to_delete)))
        if confirm:
            self.delete_flow_rules(to_delete)
            return
        else:
            return self.list_flow_rules(to_delete)

    def add_flow_rule(self):
        questions = [
            {
                'type': 'input',
                'name': 'src',
                'message': "Enter source IPv6 address or subnet",
                'default': "::/0",
                'validate': self.validate_ip_address,
                'filter': lambda val: ipaddress.IPv6Network(val),
            },
            {
                'type': 'input',
                'name': 'dst',
                'message': "Enter destination IPv6 address or subnet",
                'default': "::/0",
                'validate': self.validate_ip_address,
                'filter': lambda val: ipaddress.IPv6Network(val),
            },
            {
                'type': 'input',
                'name': 'src-port',
                'message': "Enter source port. Leave blank to match any port",
                'validate': self.validate_int,
                'filter': lambda val: None if val is "" else int(val),
            },
            {
                'type': 'input',
                'name': 'dst-port',
                'message': "Enter destination port. Leave blank to match any port",
                'validate': self.validate_int,
                'filter': lambda val: None if val is "" else int(val),
            },
            {
                'type': 'input',
                'name': 'prio',
                'message': "Enter optimization priorty of this flow (Should be >0, larger numbers mean higher priority)",
                'default': "1",
                'validate': lambda v: v is not "" and self.validate_int(v),
                'filter': lambda val: int(val),
            },
        ]

        answers = prompt(questions)
        if len(answers) != 0:
            self.flow_rules.add(
                FlowRule(
                    src=answers['src'],
                    dst=answers['dst'],
                    src_port=answers['src-port'],
                    dst_port=answers['dst-port'],
                    priority=answers['prio'],
                ))

    def manage_flow_rules(self):
        options = [
                'Add Flow Rule',
                {
                    'name': 'List or Delete Flow Rules',
                    'disabled': "No flow rules defined" if len(self.flow_rules) == 0 else False,
                },
                'Delete All Flow Rules',
                'Go Back',
            ]
        management = [{
            'type': 'list',
            'name': 'choice',
            'message': 'Flow Rule Management',
            'choices': options,
            'default': options[-1], # "Go Back"
        }]
        answer = prompt(management)
        if 'choice' not in answer:
            print("Something went wrong getting the response. Try using the keyboard instead of the mouse?")
            return self.manage_flow_rules()
        answer = answer['choice']

        if answer == 'Add Flow Rule':
            self.add_flow_rule()
            return self.manage_flow_rules()
        elif answer == 'List or Delete Flow Rules':
            self.list_flow_rules()
            return self.manage_flow_rules()
        elif answer == 'Delete All Flow Rules':
            self.flow_rules.clear()
            return self.manage_flow_rules()
        else:
            return

    def get_latest_live_netinfo(self, default_bandwidth:int, default_delay:int) -> netdiff.NetJsonParser:
        """
        Query the controller of the latest live topology
        """
        if self.debug_topology:
            print("Using debugging topology")
            return self.debug_topology
        url = self.controller_url_template.format(endpoint=self.GET_TOPOLOGY_ENDPOINT)
        response = requests.get(url)
        response.raise_for_status()
        data = json.loads(response.content)

        parser = netdiff.NetJsonParser(data=data)

        for link in parser.graph.edges():
            # 'bandwidth' and 'delay' are optional keys for the graph supplied by the server, but we do need it. So assign a default value if it's missing.
            if not 'bandwidth' in link:
                parser.graph.edges[link]['bandwidth'] = default_bandwidth
            if not 'delay' in link:
                parser.graph.edges[link]['delay'] = default_delay

        return parser

    def get_latest_live_traffic(self, netinfo: netdiff.NetJsonParser, time_window, default_priority: int=1):
        """
        This collects the data stored by netsa. This requres that this demo is being run on the netsa collector node

        Returns the TrafficMatrix ready for use by the optimizer as well as a same-order list of source_ip and dest_ip for
        real rule creation
        """
        netinfo_file: str = "/tmp/live-netinfo.json"
        if self.debug_traffic:
            print("Using debug traffic")
            traffic_file = self.debug_traffic
        else:
            # Shell out to the netsa reader
            # If only there were a library which could have done this for me...
            traffic_file = "/tmp/live-traffic.json"
            subprocess.check_call([
                "./netsa-trafficmatrix-runner.sh",
                "--type=all",
                f"--output-file={traffic_file}",
                f"--time-window={time_window}"])

        # The shell also *writes* to a file, so now we want to get that back
        tm = TrafficMatrix()
        orig_ips = []
        with open(traffic_file, "r") as file:
            data = json.load(file)
            for line in data:
                source_ip = line['source_ip']
                dest_ip = line['dest_ip']
                sPort = line['sPort']
                dPort = line['dPort']
                matches: List[FlowRule] = []
                for rule in self.flow_rules:
                    is_match = True
                    if not (rule.src_port is None or rule.src_port == sPort):
                        is_match = False
                    if not (rule.dst_port is None or rule.dst_port == dPort):
                        is_match = False
                    if not (source_ip in rule.src):
                        is_match = False
                    if not (dest_ip in rule.dst):
                        is_match = False
                    if is_match:
                        matches.append(rule)

                if len(matches) == 0:
                    priority = default_priority
                else:
                    matches.sort()
                    most_specific_match = matches[0]
                    print(f"Matched flow {line} with {most_specific_match}")
                    priority = most_specific_match.priority

                bandwidth = line['bandwidth_req'] # TODO: Take time-average

                source_ip = ipaddress.IPv6Address(line['source_ip'])
                dest_ip = ipaddress.IPv6Address(line['dest_ip'])
                source_id = DemoRunner.lookup_label_by_ip(netinfo.graph, source_ip)
                dest_id = DemoRunner.lookup_label_by_ip(netinfo.graph, dest_ip)

                if source_id is None or dest_id is None:
                    # Throw this line away, since it isn't something we know what to do with
                    # This could be, for instance, IPv6 neighbor advertisemsnt packets or other
                    # non-interesting traffic which uses fe80 (link-local) addresses
                    continue

                entry = TrafficMatrixEntry(
                    source_id = source_id,
                    dest_id = dest_id,
                    bandwidth_req = bandwidth,
                    criticality = priority,
                    sport= line['sPort'],
                    dport=line['dPort'],
                )
                tm.append(entry)
                orig_ips.append((source_ip, dest_ip))
                pass

        return tm, orig_ips

    def install_midpoints(self,
                          traffic: TrafficMatrix,
                          real_addresses: List[Tuple[ipaddress.IPv6Address, ipaddress.IPv6Address]],
                          topology: networkx.Graph,
                          new_midpoints: List[int]):
        """
        Delete all currently defined rules in the network and add new ones for the new midpoints

        It might be more efficient to not delete all and then install all midpoints, but this is much less error-prone
        """
        assert len(traffic) == len(new_midpoints), "Do not have exactly one midpoint per traffic entry"
        ip_to_dpid = self._get_all_dpids()

        pushable_rules: List[PushableFlowRule] = []
        for idx in range(len(traffic)):
            flow: TrafficMatrixEntry = traffic[idx]
            src_ip, dst_ip = real_addresses[idx]
            midpoint = new_midpoints[idx]
            midpoint_ip = self.lookup_address_for_node(topology=topology, src_node=flow.source_id, dst_node=midpoint)
            print(f"Using midpoinrt {midpoint_ip} for flow starting at {src_ip}:{flow.sport} going to {dst_ip}:{flow.dport}")
            # TODO: We should really only push the flow rule to probably one router, but it is tricky to figure out which one. Push to all, optimize later.
            for dpid in ip_to_dpid.values():
                pushable_rules.append(PushableFlowRule(dpid=dpid, midpoint=midpoint_ip, src=src_ip, dst=dst_ip, sport=flow.sport, dport=flow.dport))

        # Clear the network
        self._remove_all_paths()
        # Push new midpoint rules
        for rule in pushable_rules:
            self._install_single_path(rule)

        pass

    def run_optimizer(self, live_traffic_window: int=300, default_bandwidth: int=None, default_delay=1):
        if default_bandwidth is None:
            default_bandwidth = self.default_bandwidth
        question = [{
            'name': 'duration',
            'type': 'input',
            'message': 'For how long would you like to run the optimizer? (Milliseconds)',
            'validate': lambda v: v is not "" and self.validate_int(v),
            'filter': lambda val: int(val),
        }]

        optimization_time = prompt(question)['duration']

        print("Getting live network topology")
        netinfo = self.get_latest_live_netinfo(default_bandwidth=default_bandwidth, default_delay=default_delay)
        topology = netinfo.graph
        print(f"Getting live traffic for the last {live_traffic_window} seconds")
        traffic, real_addresses = self.get_latest_live_traffic(netinfo, time_window=live_traffic_window)

        print("Setting up optimizer")
        all_links = topology.edges()
        ospf_link_weights = {}
        # Assign ospf weights as the inverse of the link capacity
        for link in all_links:
            ospf_link_weights[link] = 1.0 / all_links[link]['bandwidth']

        print("Running Optimizer")
        router = CriticalReroute(graph=topology, ospf_link_weights=ospf_link_weights, verbose=True)

        midpoints: List[int]
        if self.current_midpoints:
            print("Reusing result midpoints cached from last run")
            midpoints = self.current_midpoints
        else:
            print("Using OSPF midpoints to seed algorithm")
            midpoints = router.get_ospf_midpoints(traffic)

        optimized_midpoints, unused, unused = router.calculate_midpoint(
            traffic_matrix=traffic,
            initial_midpoints=midpoints,
            ignore_criticality=False,
            runtime_milliseconds=optimization_time,
            num_iterations=-1,
        )
        print("Optimizer finished - Installing midpoints")
        self.install_midpoints(traffic=traffic, real_addresses=real_addresses, topology=topology, new_midpoints=optimized_midpoints)

    def prompt_remove_all_paths(self):
        question = [{
            'name': 'confirm',
            'type': 'confirm',
            'message': 'This will remove all optimized midpoints from the network and the local cache. Continue?',
            'default': False,
        }]

        confirm_delete = prompt(question)['confirm']

        if not confirm_delete:
            return
        self._remove_all_paths()

    def main_body(self) -> bool:
        options = [
            "Manage flow rules",
            "Run Optimizer",
            "Remove Optimized Paths",
            "Exit",
        ]
        control = [
            {
                'type': 'list',
                'name': 'control-panel',
                'message': 'Please select a command',
                'choices': options,
                'default': options[-1], # "Exit"
                'filter': lambda val: val.lower(),
            },
        ]

        answers = prompt(control)
        if 'control-panel' not in answers:
            print("Something went wrong getting the response. Try using the keyboard instead of the mouse?")
            return True

        answer = answers['control-panel']

        if answer == 'manage flow rules':
            self.manage_flow_rules()
        elif answer == 'run optimizer':
            self.run_optimizer()
        elif answer == 'remove optimized paths':
            self.prompt_remove_all_paths()
        elif answer == 'exit':
            return False
        return True


def main():
    parser = argparse.ArgumentParser("Run an interactive view of Critical Reroute using live data")
    parser.add_argument("--controller", action='store', type=str, required=True,
                        help="Hostname or IP address of the node running the segment-routing-sdn-controller")
    parser.add_argument("--debug-topology", action='store', type=str, required=False, help=argparse.SUPPRESS,
                        #"Use this topology for running the demo, rather than pulling from live data. For debugging."
                        )
    parser.add_argument("--debug-traffic", action='store', type=str, required=False, help=argparse.SUPPRESS,
                        #"Use this traffic matrix for running the demo, rather than pulling from live data. For debugging."
                        )
    parser.add_argument("--default-bandwidth", action='store', type=int, default=1250000,
                        help="Bandwidth value to assume if no value is returned by the server (Bytes/s)")
    parser.epilog = "This program is supposed to work using either mouse or keyboard, but mouse seems to be broken"

    args = parser.parse_args()

    runner = DemoRunner(
        management_host=args.controller,
        debug_topology=args.debug_topology,
        debug_traffic=args.debug_traffic,
        default_bandwidth=args.default_bandwidth,
    )

    while runner.main_body():
        pass


class RouteNotFoundError(Exception):
    pass

if __name__ == "__main__":
    main()