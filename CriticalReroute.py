#!/usr/bin/env python3

# Copyright (C) 2017 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function

from collections import namedtuple

from datetime import datetime
from deap import base
from deap import creator
from deap import tools
import networkx as nx
import math
import random

from typing import List, Dict

from TrafficMatrixGenerator import TrafficMatrix

# A Cost is what the cost_function outputs given an assignment of midpoints
Cost = namedtuple('Cost', ['overload_factor', 'latency_factor', 'path_length_factor'])

# A trace is a while-running output of the optimization, reporting stats about the gene pool
Trace = namedtuple('Trace', ['iteration', 'best', 'worst',  'average', 'stddev'])

# An update report is a note that a better solution has been found and the costs associated with the new solution
UpdateReport = namedtuple('UpdateReport', ['iteration', 'elapsed_time', 'new_cost'])

# A Link is represented as a tuple of source and destination
# Links are assumed to be unidirectional!
Link = namedtuple('Link', ['source', 'dest'])

# Link Loads are used as part of computing the cost function
LinkLoad = namedtuple('LinkLoad', ['available_capacity', 'criticality', 'carried_flows'])

# Individual Diffs are a tuple of the flow index and the parent's old assignment
IndividualDiff = namedtuple('IndividualDiff', ['flow_index', 'old_value'])

class CriticalReroute(object):
    VERSION = "critical-reroute-20190304" # Update this whenever the optimizer significantly changes
    DEFAULT_LINK_CAPACITY = 5000  # Let's say each unidirectional link can support 5,000 traffic 'units'. MB/s?

    def __init__(self, graph, ospf_link_weights, verbose=True):
        """ Create a new CriticalReroute solver

        :param graph:
        :type graph: networkx.Graph
        :param ospf_link_weights: Mapping of links to their ospf costs # TODO: Remove this parameter
        """
        self.shortest_path_dict = {}
        self.graph = graph
        self.link_capacities = {edge: self.graph.adj[edge[0]][edge[1]]['bandwidth'] for edge in graph.edges()}
        if not self.graph.is_directed():
            # Need backwards lookup of link dictionary
            backwards_capacities = {(edge[1], edge[0]): self.graph.adj[edge[0]][edge[1]]['bandwidth'] for edge in graph.edges()}
            self.link_capacities.update(backwards_capacities)
        self.ospf_link_weights = ospf_link_weights
        for edge in ospf_link_weights:
            self.graph.edges[edge]['weight'] = ospf_link_weights[edge]
        self.verbose=verbose
        self.trace = [] # Keep a list of everything interesting that happens, ordered by iteration

        # Create a class called MidpointsFitness which inherits deap.base.Fitness
        # This fitness definition will be used with CriticalReroute.cost_function, so the triple of weights
        # specifies to *minimize* overload_factor, latency_factor, and path_length_factor
        # Negative weights indicate that the optimizer should minimize rather than maximum (Maximize a negative value)
        creator.create("MidpointsFitness", base.Fitness, weights=(-1, -1 , -1))
        # Create a class called MidpointsIndividual which inherits from list and has an attribute called fitness
        creator.create("MidpointsIndividual", list, fitness=creator.MidpointsFitness)
        # Create a class called MidpointsPopulation which inherits from list
        creator.create("MidpointsPopulation", list)

    def verbose_print(self, str):
        """
        Print the string to stdout if it this instance is set to be verbose
        """
        if self.verbose:
            print(str)

    def get_ospf_midpoints(self, traffic: TrafficMatrix) -> List[int]:
        initial_midpoints: List[int] = [None] * len(traffic)  # Create an empty list
        for flow_index in range(0, len(traffic)):
            initial_router = traffic[flow_index].source_id
            end_router = traffic[flow_index].dest_id
            ospf_path_matrix = self.OSPF_calculator(initial_router)
            if not end_router in ospf_path_matrix:
                raise OSPF_Path_Error("Unable to find path between {} and {}".format(initial_router, end_router))
            ospf_path = self.OSPF_calculator(initial_router)[end_router]
            # As a heuristic, take the middle OSPF path node, and declare that as the initial midpoint
            ospf_midpoint = ospf_path[len(ospf_path)//2]
            initial_midpoints[flow_index] = ospf_midpoint
        return initial_midpoints

    def OSPF_calculator(self, src_router_id):
        """Find the shortest path from the src node to every other node
        TODO: Deal with equal-cost, multi-path situations
        (Even better, deal with non-equal-cost, multi-path situations, but that sounds like a heck of a lot of work)

        :param src_router_id:
        :type src_router_id: int
        :return: Dictionary of destination router IDs and their path from the source
        """

        if src_router_id in self.shortest_path_dict:
            return self.shortest_path_dict[src_router_id]

        # The local cache has not been initialized
        # TODO: Unsure if this is necessary or if networkx already provides caching
        self.shortest_path_dict[src_router_id] = nx.single_source_dijkstra_path(self.graph, source=src_router_id, weight='weight')
        return self.shortest_path_dict[src_router_id]

    @staticmethod
    def diff_individual(ind1, ind2) -> List[IndividualDiff]:
        """
        Compare two individuals. Report the indicies where the first individual differs from the second and the value of the second
        individual at that point
        """
        toReturn = []

        assert(len(ind1) == len(ind2))

        for idx in range(len(ind1)):
            if not ind1[idx] == ind2[idx]:
                toReturn.append(IndividualDiff(idx, ind2[idx]))

        return toReturn

    def cost_function(self, midpoints, traffic_matrix, ignore_criticality) -> Cost:
        """Determine the cost of routing the flow on this path

        For each flow:
        Determine the links used by the flow (from the segment-routing midpoint and OSPF)
        Compute the load at each link, determined by the bandwidth requirement of each flow
        For any link which is overloaded, take that overload value and multiply by the
        criticality of the most critical flow on that link
        The cost of each genetic sequence is the sum of those weighted overloads
        For tiebreaking, this method also returns the total number of extra links used
        compared to opsf-routing to encourage the algorithm to find a shorter path if many will work

        :param individual: List of router IDs representing midpoints of the flows in traffic_matrix
        :param traffic_matrix: List of flows
        :param ignore_criticality: Whether the criticality should be used to weight flow costs
        :return: A Cost tuple
        """
        overloads = []  # Overload of each link, multiplied by the criticality of the most-critical flow using the link
        latencies = []  # Latency of each flow, multiplied by its criticality, which will be summed later
        path_length_factor = 0  # For tie-breaking paths, keep track of the total number of links used compared to shortest-path

        # Copy the link costs list, adding a field for storing the max criticality used at each link
        link_loads = {}
        for link in self.link_capacities.keys():
            # Default criticality to zero so the first loaded flow will fill it in
            link_loads[link] = list((self.link_capacities[link], 0))

        # Calculate the link loads, criticalities, and the path length
        # Loop runs O(F) times
        # The body contains a O(log(L)) term
        # So, this loop is O(F*log(L))
        for flow_index in range(0, len(traffic_matrix)):
            flow = traffic_matrix[flow_index]
            flow_midpoint = midpoints[flow_index]
            # OSPF Lookup is asymptotically O(1) since that is cached after the first round of lookups
            first_half_path = self.OSPF_calculator(flow.source_id)[flow_midpoint]
            second_half_path = self.OSPF_calculator(flow_midpoint)[flow.dest_id]
            segmented_path = first_half_path + second_half_path[1:]
            # Also look at the pure OSPF path, to see how much longer the segment-routed path may be
            # O(1)
            ospf_path = self.OSPF_calculator(flow.source_id)[flow.dest_id]
            path_length_factor += len(segmented_path) - len(ospf_path)
            latency = 0
            # Run once for every link in this flow
            # For very simple networks (a stick with no branches), this could be L (the flow uses all links in the network)
            # Normally, it will be something less than L
            # Starting from the "stick" example, imagine that a path can branch at any point, the number of such branches
            # is based on the branching factor of the network. Assume that, on average, all possible paths through the network
            # are the same length. Therefore, there are Z^b Z-length paths, so Z, the path length = log_{b+1}(L)
            # Thus, this loop is run O(log(L)) times
            for path_index in range(0, len(segmented_path) - 1):
                link = (segmented_path[path_index], segmented_path[path_index + 1])
                # Update the bandwidth used on this link
                link_loads[link][0] -= flow.bandwidth_req
                # Check if we should update the criticality
                if link_loads[link][1] < flow.criticality:
                    link_loads[link][1] = flow.criticality

                # Compute the latency of this flow
                latency += self.graph.edges[link]['delay'] * flow.criticality
            latencies.append(latency)

        # Compute the overload factor
        # O(L)
        for link in link_loads:
            overload, criticality = link_loads[link]
            overload *= -1
            if overload < 0:
                # If the resulting link load was non-negative, there was still bandwidth available
                # Ignore this link
                continue
            if ignore_criticality:
                overloads.append(overload)
            else:
                overloads.append(overload * criticality)

        # O(L)
        # However, F is likely much larger than L, so F*log(L) is likely to dominate
        overload_factor = math.fsum(overloads)
        # O(F)
        latency_factor = math.fsum(latencies)

        return Cost(overload_factor=overload_factor,
                    latency_factor=latency_factor,
                    path_length_factor=path_length_factor,
                    )

    def mutate_random(self, individual, all_routers, indpb):
        """
        Randomly mutate an individual by, with a probability indpb for each midpoint it contains,
        uniformly selecting a random midpoint from all possible routers

        :param individual: Sequence to mutate - modified in-place
        :param all_routers: List of all possible routers for choosing a midpoint
        :param indpb: Independent Probability of mutating a specific attribute
        :return: None - param individual is modified in-place
        """
        for index in range(len(individual)):
            chance = random.random()
            if chance < indpb:
                individual[index] = random.choice(all_routers)

    def mutate_neighbor(self, individual, graph, indpb):
        """
        Mutate an individual by, with a probability indpb for each midpoint it contains,
        selecting a random neighbor to replace that midpoint

        :param individual: Sequence to mutate - modified in-place
        :param graph: G from ovs-srv6/sdn_controller/TE/structs.py
        :param indpb: Probability of mutating a particular attribute
        :return: None - param individual is modified in-place
        """
        # Outer loop runs O(F) times, expected inner loop cost is 1*a
        # So, exptected cost of one call of this function is O(F*a)
        for index in range(len(individual)):
            chance = random.random()
            if chance < indpb:
                # Enumerating neighbors should be O(1) because NetworkX appears to be adjacency dictionaries
                neighbors = [n for n in nx.all_neighbors(graph, individual[index])]
                # O(1)
                individual[index] = random.choice(neighbors)

    def calculate_midpoint(self,
                           traffic_matrix,
                           initial_midpoints,
                           ignore_criticality,
                           gene_pool_size=10,
                           num_iterations=10000,
                           crossover_chance=0.75,
                           child_mutation_chance=0.25,
                           allele_mutation_chance=0.1,
                           runtime_milliseconds: int=-1,
                           ):
        """Calculate the ideal segment routing midpoints for all edge nodes

        This midpoint should be calculated for each node such that:
            - The amount of bandwidth requested for each critical edge-to-edge
            flow exceeds the available bandwidth by the minimum amount
            - The amount of bandwidth requested for each non-critical edge-to-edge
            flow exceeds the available bandwidth by the minimum amount
            - Critical flows are given a 'criticality' weight, which is a multiplier
            on how expensive it is for that flow to be on an overloaded link
                - Different flows might be more or less critical, so assigned
                different criticality weight!

        It is recommended to include at least one of num_iterations or runtime_milliseconds, otherwise the optimizer
        will never return!

        :param gene_pool_size: Number of midpoint assignments in each generation
        :param traffic_matrix: List of edge-to-edge flows, specified by their source,
        destination, required bandwidth, and criticality
        :param initial_midpoints: Ordered list of initial midpoints, where each entry
        corresponds to the same-index flow in traffic_matrix
        :param ignore_criticality: Whether this calculator should specially treat critical flows
        :param num_iterations: Number of genetic lotteries to run. Ignored if negative.
        :param crossover_chance: Chance for two chosen genomes to recombine to create a child rather than copy a parent
        :param child_mutation_chance: Chance for a newly-created child to be mutated
        :param allele_mutation_chance: Chance for a particular allele to be mutated when a child is selected to be mutated
        :param runtime_milliseconds: Time the optimizer should be allowed to run. Ignored if negative.
        :return:
        """

        self.trace = []

        # In order to determine convergence, keep a rolling average over the last 10% of iterations
        # Initialize the rolling average by assuming the last iterations all had the same cost
        # as the initial midpoint values
        rolling_average_size = num_iterations / 10
        rolling_average_cost, unused, unused = self.cost_function(initial_midpoints, traffic_matrix, ignore_criticality)

        # The genetic population is a self.gene_pool_size-long array of sequences
        # Each sequence is an array where each index matches a requested flow
        # Each element of a sequence is a router ID representing the segment-routing midpoint
        genetic_population = []

        # Initialize the DEAP toolbox which will contain all methods for handling the genetic algorithm
        genetic_toolbox = base.Toolbox()

        # Create the function alias "midpoints_individual" which constructs a single individual using initial_midpoints
        genetic_toolbox.register("midpoints_initialize", creator.MidpointsIndividual, initial_midpoints)
        genetic_toolbox.register("midpoints_individual", tools.initIterate,
                                 creator.MidpointsIndividual, genetic_toolbox.midpoints_initialize)

        # Create a function alias "midpoints_population" which constructs a MidpointsPopulation of midpoints_individuals
        genetic_toolbox.register("midpoints_population", tools.initRepeat,
                                 creator.MidpointsPopulation, genetic_toolbox.midpoints_individual)

        # Register the cost function as "evaluate"
        genetic_toolbox.register("evaluate", self.cost_function, traffic_matrix=traffic_matrix, ignore_criticality=ignore_criticality)

        # Register the crossover function - Change this to play with different ones!
        # The mating function randomly selectes two points (O(1)) then copies parts of the first into the second and
        # parts of the second into the first, for a total of F alleles copied
        genetic_toolbox.register("mate", tools.cxTwoPoint)

        # Register the mutation function
        genetic_toolbox.register("mutate", self.mutate_neighbor, graph=self.graph, indpb=allele_mutation_chance)

        # Register the selection function
        # TODO: selRoulette cannot be used for minimization -- use inversion tricks from initial implementation and test?
        # Tournament selection randomly selects tournsize individuals (O(1)) then compares their fitness (O(1))
        genetic_toolbox.register("select", tools.selTournament, tournsize=3)

        pop = genetic_toolbox.midpoints_population(n=gene_pool_size)

        best_individual = pop[0]

        fitnesses = list(map(genetic_toolbox.evaluate, pop))

        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit

        start_time = datetime.now()
        iteration = 0
        # Main loop -- Everything of interest happens within
        # Big-O calculation variables:
        # P: Population Size
        # F: Number of flows being routed
        # L: Number of links in the network
        # c: Crossover Chance
        # m: Chance that a child is selected to be mutated
        # a: Allele Mutation Chance
        while iteration != num_iterations:
            iteration += 1

            elapsed_time = datetime.now() - start_time

            if runtime_milliseconds > 0 and elapsed_time.total_seconds() * 10**3 >= runtime_milliseconds:
                break

            # Select the next generation by running the selection function registered above
            # O(P) * O(1)
            offspring = genetic_toolbox.select(pop, len(pop))
            # Deep copy the selected offspring (else they still refer to the individuals in pop)
            # O(P)
            offspring = list(map(creator.MidpointsIndividual, offspring))

            # Apply crossover
            # Outer loop runs P/2 times
            # Inner function is O(1) when it does no crossover, and O(F) when crossover happens
            # So, the expected cost of this loop is  O(F*c * P)
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < crossover_chance:
                    # O(F)
                    genetic_toolbox.mate(child1, child2)
                    # O(1)
                    del child1.fitness.values
                    del child2.fitness.values

            # Apply mutation
            # Outer loop runs P times
            # Inner loop cost is O(1) when a child is not selected for mutation and O(F*a) when selected
            # So, the expected cost of this block is O(P*m*F*a)
            for mutant in offspring:
                if random.random() < child_mutation_chance:
                    # O(F*a)
                    genetic_toolbox.mutate(mutant)
                    # O(1)
                    del mutant.fitness.values

            # Recalculate fitnesses for changed individuals
            # In practice, this will almost always be done for all individuals, so assume O(P)
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            # O(P) individuals times O(F*log(L)) to compute the cost
            # O(P*F*log(L))
            fitnesses = map(genetic_toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit

            pop[:] = offspring

            # I assume this is a linear search since the population list is un-sorted
            # O(P)
            best_current = tools.selBest(pop, 1)[0]

            if best_individual.fitness < best_current.fitness:
                self.verbose_print("Old best fitness: {}, weighted: {}".format(str(best_individual.fitness), sum(best_individual.fitness.wvalues)))
                self.verbose_print("New best fitness: {}, weighted: {}".format(str(best_current.fitness), sum(best_current.fitness.wvalues)))
                self.trace.append(UpdateReport(iteration=iteration, elapsed_time=elapsed_time.total_seconds(), new_cost=Cost(*best_current.fitness.values)))
                best_individual = best_current

            # Updating the convergence average
            # Assume that all the past averaged costs were equal, so an individual cost from the past
            # is exactly the average, then use that number n - 1 times where n is the number of
            # datapoints and add in the new value to get the new average
            best_cost = best_current.fitness.values[0]
            rolling_average_cost = rolling_average_cost * (rolling_average_size - 1)
            rolling_average_cost += best_cost
            rolling_average_cost = rolling_average_cost / float(rolling_average_size)

            if iteration % (num_iterations / 10) == 0:
                self.verbose_print("Iteration: " + str(iteration))
                # Gather all the fitnesses in one list and print the stats
                fits = [ind.fitness.values for ind in pop]

                length = len(pop)
                # Calculate the average of each field of the fitness
                mean = tuple(map(lambda x: sum(x)/len(x), zip(*fits)))
                sqsum = tuple(map(lambda column: sum(x*x for x in column), zip(*fits)))
                std = tuple(map(lambda sqsum, avg: abs(sqsum/length - avg**2) ** 0.5, sqsum, mean))

                # Since the weights are negative, the ideas min and max are reversed
                best = best_current.fitness
                worst = tools.selWorst(pop, 1)[0].fitness
                self.verbose_print("  Best %s" % best)
                self.verbose_print("  Worst %s" % worst)
                self.verbose_print("  Avg %s" % str(mean))
                self.verbose_print("  Std %s" % str(std))

                self.trace.append(Trace(iteration=iteration,
                                        best=Cost(*best.values),
                                        worst=Cost(*worst.values),
                                        average=Cost(*mean),
                                        stddev=Cost(*std)))

        # Find the best sequence
        best_result = tools.selBest(pop, 1)[0]
        best_sequence, best_cost = best_result, best_result.fitness.values

        return best_individual, Cost(*best_individual.fitness.values), rolling_average_cost

class OSPF_Path_Error(Exception):
    pass
