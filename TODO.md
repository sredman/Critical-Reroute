* Need to have the input graphs annotated with bandwidth of the links and
  make use of that information
  * This is mostly an issue of the orchestrator not automatically sniffing that up
* Better parameters for Genetic Algorithm
    * This is a never-ending project, and varies based on the inputs, so some
    "smart" solution might be in order. That sounds like a PhD project.
* Make GA report that it has/has not converged when terminating
    * This is useful in general, but since we are always running in a time-limited
    situation, it isn't especially useful to us
* Better heuristic for original midpoints selection
    * OSPF-inspired midpoints?
        * Walk along flow's OSPF path, find central one?
        * Choose randomly from steps in the OSPF path?
    * A Declarative and Expressive Approach to Control Forwarding Paths in Carrier-Grade Networks, section 4.3?
    * Greedily assign flows in order of criticality, then optmize?
    * Should heuristic define entire gene pool, or one gene sequence which is tweaked and used
* Scale GA parameters as algorithm runs?
    * Higher mutation chance early on?
    * Essentially multi-solution simulated annealing, which is a solid strategy
* Improve algorithm runtime:
    * Multi-thread?
    * Multi-machine?
    * More weighting for better solutions?
        * By how much do the costs in a gene pool typically vary? Presumably not much
