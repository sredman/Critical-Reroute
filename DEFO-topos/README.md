This folder contains topologies published as part of the DEFO project,
which is an optimizer solving similar goals to Critical Reroute.

The DEFO website is here: https://sites.uclouvain.be/defo/

The two publications describing DEFO are "A Declarative and Expressive
Approach to Control Forwarding Paths in Carrier-Grade Networks" by
Renaud Hartert, et al. and Renaud Hartert's PhD thesis published by the
Université Catholique de Louvain, available here:
https://dial.uclouvain.be/pr/boreal/object/boreal:207460

The Python executable, "DEFO-to-NetJson-converter.py" is part of Critical
Reroute, and converts the file format of the graph and demand files shared
by DEFO into json format compatible with the rest of Critical Reroute.
The converter relies on data structure definitions from the rest of Critical
Reroute, so you will probably have to extend your PYTHONPATH environment
variable to contain the project root before the converter will run.