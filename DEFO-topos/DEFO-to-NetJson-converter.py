#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
from collections import namedtuple
from netdiff import NetJsonParser
import json

from TrafficMatrixGenerator import TrafficMatrix, TrafficMatrixEntry

from typing import List


# This should match the version in DataGatherer to which this converter complies
# See DataGatherer.py for more information
VERSION = "20190129"


Node = namedtuple('Node', ['label', 'x', 'y'])
Edge = namedtuple('Edge', ['label', 'src', 'dst', 'weight', 'bandwidth', 'delay'])
Demand = namedtuple("Demand", ['label', 'src', 'dst', 'bandwidth'])


def parse_node(line: str) -> Node:
    return Node(*line.split())


def parse_edge(line: str) -> Edge:
    # The values in the incoming text are strings (of course)
    # Convert the numbers to numbers before returning
    string_edge = Edge(*line.split())._asdict()
    string_edge['weight'] = int(string_edge['weight'])
    string_edge['bandwidth'] = int(string_edge['bandwidth'])
    string_edge['delay'] = int(string_edge['delay'])
    return Edge(**string_edge)


def parse_demand(line: str, nodes: List[Node]):
    demand = Demand(*line.split())
    src = nodes[int(demand.src)]
    dst = nodes[int(demand.dst)]
    return TrafficMatrixEntry(source_id=src,
                              dest_id=dst,
                              bandwidth_req=int(demand.bandwidth),
                              criticality=1)

def parse_graph(filename: str) -> (NetJsonParser, List[Node]):
    mode = None
    skip = False
    outgraph = NetJsonParser(data={"type": "NetworkGraph",
                                   "protocol": "static",
                                   "version": None,
                                   "metric": None,
                                   "nodes": [],
                                   "links": []},
                             directed=True)

    nodes = [] # Need an ordered list of nodes to retrieve src and dest for edges

    with open(filename, "r") as input:
        for line in input:
            if skip:
                skip = False
                continue
            line = line.strip()
            if len(line) < 1:
                continue # Skip blank lines
            if line in ["NODES", "EDGES"]:
                mode = line
                skip = True # Following the mode is a line defining the fields. We don't care to parse that.
                continue

            if mode == "NODES":
                node = parse_node(line)
                attrs = node._asdict()
                outgraph.graph.add_node(node.label, **attrs)
                nodes.append(node.label)

            elif mode == "EDGES":
                edge = parse_edge(line)
                attrs = edge._asdict()
                src = nodes[int(attrs.pop('src'))]
                dst = nodes[int(attrs.pop('dst'))]
                assert src in outgraph.graph.nodes
                assert dst in outgraph.graph.nodes
                outgraph.graph.add_edge(src, dst, **attrs)

            else:
                assert (False, "Undefined mode")

    return outgraph, nodes


def parse_demands(filename: str, nodes: List[Node]) -> TrafficMatrix:
    mode = None
    skip = False
    traffic_matrix = TrafficMatrix()

    with open(filename, "r") as input:
        for line in input:
            if skip:
                skip = False
                continue
            line = line.strip()
            if len(line) < 1:
                continue # Skip blank lines
            if line in ["DEMANDS",]:
                mode = line
                skip = True # Following the mode is a line defining the fields. We don't care to parse that.
                continue

            if mode == "DEMANDS":
                entry = parse_demand(line, nodes)
                traffic_matrix.append(entry)
            else:
                assert(False, "Undefined mode")
    return traffic_matrix


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Convert a test case downloaded from https://sites.uclouvain.be/defo/ to NetJSON")
    parser.add_argument("--in-graph", action='store', type=str, required=True,
                        help="Path to the graph file to parse")
    parser.add_argument("--in-demands", action='store', type=str, required=False,
                       help="Path to the demands file corresponding to this graph. Defaults to the .demands file with the same name as ${in-graph}.graph")
    parser.add_argument("--out-file", action='store', type=str,
                        help="Path to the file to write as NetJSON. Defaults to ${in-file}.json")

    args = parser.parse_args()

    if args.out_file is None:
        args.out_file = str(args.in_graph).replace(".graph", "")
    if args.in_demands is None:
        args.in_demands = str(args.in_graph).replace(".graph", ".demands")

    graph, nodes = parse_graph(args.in_graph)
    traffic_matrix = parse_demands(args.in_demands, nodes)

    graph_out_filename = args.out_file + ".graph.json"

    # Write the graph to its own file
    # Graphs might come from several different sources, so it makes sense to keep them separate from
    # the rest of the test data
    with open(graph_out_filename, 'w') as outfile:
        outfile.write(graph.json())

    # Write the "rest" of the test data to a test file
    # I will need to write documentation for how these are defined once I decide
    # how to define them
    # For now, define one kind of test: define a key called "graph-type" which
    # takes the value "file"
    # When "graph-type"="file", define another key called "graph-filename" which
    # holds the filename of the key
    # All test files have a key "traffic-matrix" which holds the complete traffic matrix
    with open(args.out_file + ".cr-test.json", 'w') as outfile:
        output = {
            "graph-type": "file",
            "graph-filename": graph_out_filename,
            "traffic-matrix": [entry._asdict() for entry in traffic_matrix],
            "version": VERSION,
        }
        outfile.write(json.dumps(output))