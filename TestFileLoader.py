#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json
from pathlib import Path
import netdiff
import networkx

from TrafficMatrixGenerator import TrafficMatrix, TrafficMatrixEntry

from CriticalReroute import CriticalReroute


class TestFileLoader:
    """
    Load a test case as described by the specification in TEST-FILE-FORMAT.md
    """

    def __init__(self, filename: str):
        """
        Load the test file specified
        """
        self.test_filename = filename

        with open(filename, 'r') as input:
            data = json.load(input)

        for key in data:
            clean_key = str(key).replace("-", "_")
            self.__setattr__(clean_key, data[key])

        if self.graph_type == "file":
            self.graph = self.load_graph_from_file(self.graph_filename)
        elif self.graph_type == "generated":
            raise NotImplementedError()
        else:
            raise ValueError("Unknown graph-type: " + self.graph_type)

        self.traffic_matrix = TrafficMatrix([TrafficMatrixEntry(**entry) for entry in self.traffic_matrix])

    def load_graph_from_file(self, filename: str) -> networkx.Graph:
        if Path(filename).is_file():
            parser = netdiff.NetJsonParser(file=filename, directed=True)
        else:
            # Try to recreate the path to the graph file using the path
            # to the test file
            test_path = Path(self.test_filename)
            parts = list(test_path.parts[:-1])
            parts.append(filename)
            test_path = Path(*parts)
            parser = netdiff.NetJsonParser(file=test_path, directed=True)

        return parser.graph


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Load and run a test case matching the format specified in TEST-FILE-FORMAT.md")
    parser.add_argument("--filename", action='store', type=str, required=True,
                        help="Path to the test file to read")
    parser.add_argument("--num-iterations", action='store', type=int, default=-1,
                        help="Number of iterations to run, default=unlimited")
    parser.add_argument("--runtime-milliseconds", action='store', type=int, default=-1,
                        help="Amount of time to run the simulator")

    args = parser.parse_args()

    if args.num_iterations < 0 and args.runtime_milliseconds < 0:
       raise argparse.ArgumentError("One of num_iterations or runtime_milliseconds must be specified")

    loader = TestFileLoader(args.filename)

    print("Loaded a test file with:")
    print("A graph containing {} nodes and {} edges".format(len(loader.graph.nodes), len(loader.graph.edges)))
    print("A traffic matrix containing {} entries".format(len(loader.traffic_matrix)))
    print("Starting optimizer")

    link_capacities = {edge: loader.graph.adj[edge[0]][edge[1]]['bandwidth'] for edge in loader.graph.edges()}

    ospf_weights = {}
    # Assign ospf weights as the inverse of the link capacity
    for link in loader.graph.edges():
        ospf_weights[link] = 1.0 / link_capacities[link]

    optimizer = CriticalReroute(loader.graph, ospf_weights, verbose=True)

    loaded_initial_midpoints = hasattr(loader, "initial-midpoints")

    if loaded_initial_midpoints:
        initial_midpoints = loader.initial_midpoints
    else:
        initial_midpoints = [None] * len(loader.traffic_matrix)  # Create an empty list
        for flow_index in range(0, len(loader.traffic_matrix)):
            initial_router = loader.traffic_matrix[flow_index].source_id
            end_router = loader.traffic_matrix[flow_index].dest_id
            ospf_path = optimizer.OSPF_calculator(initial_router)[end_router]
            # As a heuristic, take the middle OSPF path node, and declare that as the initial midpoint
            ospf_midpoint = ospf_path[len(ospf_path)//2]
            initial_midpoints[flow_index] = ospf_midpoint

    best_midpoints, best_cost, rolling_average = optimizer.calculate_midpoint(loader.traffic_matrix,
                                                                              initial_midpoints,
                                                                              crossover_chance = 0.75,
                                                                              num_iterations = args.num_iterations,
                                                                              runtime_milliseconds = args.runtime_milliseconds,
                                                                              ignore_criticality=False)

    total_bandwidth = sum([entry.bandwidth_req for entry in loader.traffic_matrix])

    for flow_index in range(0, len(loader.traffic_matrix)):
        print("Src: " + str(loader.traffic_matrix[flow_index].source_id)\
              + " Dst: " + str(loader.traffic_matrix[flow_index].dest_id)\
              + " Mid: " + str(best_midpoints[flow_index])\
              + " Criticality: " + str(loader.traffic_matrix[flow_index].criticality))

    print("Total Cost: " + str(best_cost))
    print("Total Bandwidth Requested: " + str(total_bandwidth))
    print("Rolling Average: " + str(rolling_average))
    pass