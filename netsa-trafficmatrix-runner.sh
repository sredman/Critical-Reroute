#!/usr/bin/env bash
#
# Filename: ./netsa-traiffcmatrix-runner.sh
#
# Author: Simon Redman <simon@ergotech.com>
# File Created: 14.05.2019
# Last Modified: Tue 14 May 2019 03:40:59 PM MDT
# Description: 
#
#

export SILK_CLOBBER=1
source env2/bin/activate
python ./netsa-TrafficMatrix-reader.py "$@"
