#!/usr/bin/env python3

# Copyright (C) 2017 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import networkx as nx
import netdiff

from typing import Dict


def jsonize_topology(graph: nx.Graph) -> Dict:
    """
    Convert the given graph to json
    :return: NetJson representation of the passed graph
    """

    # Construct a NetJsonParser object with dummy data
    parser = netdiff.NetJsonParser(data={"type": "NetworkGraph",
                                         "protocol": "static",
                                         "version": None,
                                         "metric": None,
                                         "nodes": [],
                                         "links": []})

    # Shove our graph to convert into the hapless parser
    parser.graph = graph

    return parser.json(dict=True)


def write_topology_to_file(filename: str, graph: nx.Graph):
    """
    jsonize the passed graph then it write to the requested file
    :return: None
    """
    data = jsonize_topology(graph)

    with open(filename, 'w') as outfile:
        json.dump(data, outfile)


def deserialize_topology(data: Dict) -> nx.Graph:
    """
    Convert the given NetJson topology into an undirected Graph object
    """
    parser = netdiff.NetJsonParser(data=data)

    return parser.graph


def read_topology_from_file(filename: str) -> nx.Graph:
    """
    Convert a given file containing NetJson into an undirected Graph object
    """
    parser = netdiff.NetJsonParser(file=filename)

    return parser.graph
