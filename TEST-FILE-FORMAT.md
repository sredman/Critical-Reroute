# Critical Reroute Test File Format

This document describes the file format used by this project to store
test parameters for later replay and retrieval

Test files are any file which has a .cr-test.json extension

Test files are JSON formatted key-value pairs as defined below:

## Required Keys
All test files require the following keys to be defined

- version: A string which determines the version of the test file format
           complied to. See the version history section for more detail.

- graph-type: A string which defines the source of the (topology) graph
              used in this test. Defining this key requires other keys
              to be defined with more information. See the graph type
              section for more detail.

- traffic-matrix: A list of traffic matrix entries as defined in
                  TrafficMatrixGenerator.py

## Optional Keys
All test files are allowed to define the following keys:

- initial-midpoints: A vector of midpoints which must be the same length
                     as the traffic-matrix key. When the test is started,
                     these midpoints will be used instead of the defaults.
                     The midpoint for a particular flow will be assigned
                     by using the midpoint which is at the same index in
                     the initial-midpoints as the flow is in the
                     traffic-matrix

### Graph Type
The following graph types are defined. Once a graph type is defined, the
additional keys it defines are also required

- file: Indicate that this graph should be loaded from a file. Defines
        the following key:
  - graph-filename: The name of the file containing the graph to load
- generated: Indicate that this graph was generated. This is the assumed
             value for versions 20180404 and older. Defines the following
             keys:
  - topology-generator-version: A string indicating which generator was
                                used
  - topology-generator-params: A key-value paring of all parameters given
                               to the used topology generator
  - graph: If the generator is non-deterministic, the graph key should
           hold a representation of the graph generated

## Version History
- 20190325: Add "initial-midpoints" optional key
- 20190129: Add "graph-type" key, which can be "generated" or "file".
            Having the "graph-type" key defined allows requires keys to
            be defined.
            Replace underscores in key names with dashes
- 20180404: Initial version