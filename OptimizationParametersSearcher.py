#!/usr/bin/python2

# Copyright (C) 2017 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from CriticalReroute import CriticalReroute
from StarTopoGenerator import StarTopoGenerator
from TrafficMatrixGenerator import TrafficMatrixGenerator

import multiprocessing
from multiprocessing.pool import ThreadPool

import numpy as np

class OptimizationParametersSearcher:
    """Test the CriticalReroute segment-routing path-finding algorithm with a variety of
    parameters, to optimize the optimization

    """

    def __init__(self,
                 core_routers = 4, lvl1_routers = 3, lvl2_routers = 3,
                 start_pool_size=50, end_pool_size=400, pool_step_size=50,
                 start_iterations=100, end_iterations=1500, iterations_step_size=100,
                 start_mutation_chance=0.01, end_mutation_chance=0.1, mutation_step_size=0.01,
                 start_path_weight=10, end_path_weight=100, path_weight_step_size=10,
                 repetitions=10
                 ):
        # Generate one topology for all of the tests
        self.graph, self.edge_routers = StarTopoGenerator.generate(core_routers=core_routers,
                                                                   lvl1_routers=lvl1_routers,
                                                                   lvl2_routers=lvl2_routers)
        # Generate one traffic matrix for all of the tests
        self.traffic_matrix, self.total_req_bandwidth =\
            TrafficMatrixGenerator.RandomTieredTrafficMatrix(self.edge_routers,
                                                             num_entries=150,
                                                             percentage_high_bandwidth=0.2,
                                                             percentage_medium_bandwidth=0.5,
                                                             percentage_low_bandwidth=0.3,
                                                             high_bandwidth=750,
                                                             medium_bandwidth=500,
                                                             low_bandwidth=250,
                                                             criticality_percentage=0.2,
                                                             critical_criticality=5
                                                             )

        self.pool_sizes = np.arange(start_pool_size, end_pool_size, pool_step_size)
        self.iteration_vals = np.arange(start_iterations, end_iterations, iterations_step_size)
        self.mutation_vals = np.arange(start_mutation_chance, end_mutation_chance, mutation_step_size)
        self.path_weight_vals = np.arange(start_path_weight, end_path_weight, path_weight_step_size)
        self.repetitions = repetitions

        if start_path_weight == end_path_weight:
            self.path_weight_vals=[start_path_weight]

        all_links = self.graph.getAllLinks()

        self.link_capacities = {}
        for link in all_links:
            # For now, assign all links the same capacity
            self.link_capacities[link] = CriticalReroute.DEFAULT_LINK_CAPACITY;

        self.ospf_link_weights = {}
        # Assign ospf weights as the inverse of the link capacity
        for link in all_links:
            self.ospf_link_weights[link] = 1.0 / self.link_capacities[link]

    def doit(self):

        parameters = [(pool_size, iterations, mutation_chance, path_weight)
                      for pool_size in self.pool_sizes
                      for iterations in self.iteration_vals
                      for mutation_chance in self.mutation_vals
                      for path_weight in self.path_weight_vals]

        compute_pool = ThreadPool(multiprocessing.cpu_count())

        # compute_pool.map(self.doit_threaded, parameters)
        for parameter in parameters:
            for repetition in range(0, self.repetitions):
                self.doit_threaded(parameter)

    def doit_threaded(self, parameter):
        router = CriticalReroute(self.graph, self.link_capacities, self.ospf_link_weights, verbose=False)
        best_sequence, best_cost = router.calculate_midpoint(self.traffic_matrix,
                                                             gene_pool_size=parameter[0],
                                                             num_iterations=parameter[1],
                                                             mutation_chance=parameter[2],
                                                             path_weight=parameter[3])
        print "Parameters: " + str(parameter) + " Cost: " + str(best_cost)


searcher = OptimizationParametersSearcher(
    repetitions=5,
    start_path_weight=10, end_path_weight=10, path_weight_step_size=1
)
searcher.doit()