# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import datetime
import ipaddress
from json import JSONEncoder

class TrafficMatrixEntryEncoder(JSONEncoder):

    def default(self, value):
        if isinstance(value, ipaddress.IPv6Address):
            return str(value)
        elif isinstance(value, datetime):
            return str.join("T", [value.date().strftime("%Y/%m/%d"), value.time().strftime("%H:%M:%S")])
        else:
            return super(TrafficMatrixEntryEncoder, self).default(value)
