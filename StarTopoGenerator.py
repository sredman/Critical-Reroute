#!/usr/bin/env python3

# Copyright (C) 2017 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple
import networkx as nx

Node = namedtuple('Node', ['ID', 'params'])
Edge = namedtuple('Edge', ['src', 'dst', 'params'])


class StarTopoGenerator(object):
    """ This class describes a generator for a three-layer star toplogy

    The star topology has some number of core switches, each interconnected, each connected to some
    number of level 1 switches, and each level 1 switch connected to some number of level 2 switches
    Therefore, if there are x core switches, y level 1 switches per core switch, and z level 2
    switches per level 1 switch, there are a total of x*y level 1 switches and (x*y)*z level 2
    switches
    """
    NUM_CORE_ROUTERS = 4  # Number of switches in 'center' of the star
    NUM_LVL1_FANOUT = 3  # Number of switches connected to each core switch
    NUM_LVL2_FANOUT = 3  # Number of switches connected to each level 1 switch

    # Multiply these offsets to the specified level of router to give them "namespaces"
    CORE_ROUTER_ID_OFFSET = 1
    LVL1_ROUTER_ID_OFFSET = 100
    LVL2_ROUTER_ID_OFFSET = 1000

    # Every link gets the same bandwidth. MB/s?
    BANDWIDTH=5000

    # Every link gets the same delay. Let's call it milliseconds
    DELAY=10

    # Update this whenever the topology generator significantly changes
    # History:
    # star-topo-generator-20190131: Add bandwidth and delay to output graph
    # star-topo-generator-20180405: Initial version
    VERSION = 'star-topo-generator-20190131'

    @staticmethod
    def generate(core_routers: int=NUM_CORE_ROUTERS,
                 lvl1_routers: int=NUM_LVL1_FANOUT,
                 lvl2_routers: int=NUM_LVL2_FANOUT,
                 link_bandwidth: int=BANDWIDTH,
                 link_delay: int=DELAY):
        """Generate the star topology described by the passed parameters

        :param core_routers: Number of switches in 'center' of the star
        :type core_routers: int
        :param lvl1_routers: Number of switches connected to each core switch
        :type lvl1_routers: int
        :param lvl2_routers: Number of switches connected to each level 1 switch
        :type lvl2_routers: int
        :param link_bandwidth: Bandwidth assigned to every generated link
        :param link_delay: Delay assigned to every generated link
        :return: A graph object representing the described topology paired with a list of edge routers
        """
        graph = nx.DiGraph()
        edge_routers = []
        edge_params = {
            "bandwidth": link_bandwidth,
            "delay": link_delay,
        }

        for core_index in range(1, core_routers + 1):
            core_router_id = core_index * StarTopoGenerator.CORE_ROUTER_ID_OFFSET
            # Create an object to be used as our networkx node
            core_router = Node(ID=core_router_id, params={'IntraAdjs' : [], 'Prefixes' : [], 'ActiveNbs' : {}})
            graph.add_node(core_router.ID, **core_router.params)

            for lvl1_index in range(1, lvl1_routers + 1):
                lvl1_router_id = core_router_id\
                                 + lvl1_index * StarTopoGenerator.LVL1_ROUTER_ID_OFFSET
                graph.add_edge(core_router_id, lvl1_router_id, **edge_params)
                graph.add_edge(lvl1_router_id, core_router_id, **edge_params)

                for lvl2_index in range(1, lvl2_routers + 1):
                    lvl2_router_id = lvl1_router_id\
                                     + lvl2_index * StarTopoGenerator.LVL2_ROUTER_ID_OFFSET
                    graph.add_edge(lvl1_router_id, lvl2_router_id, **edge_params)
                    graph.add_edge(lvl2_router_id, lvl1_router_id, **edge_params)
                    edge_routers.append(lvl2_router_id)

        # Interconnect the core routers
        for core_index in range(1, core_routers + 1):
            for other_core_index in range(core_index + 1, core_routers + 1):
                graph.add_edge(core_index, other_core_index, **edge_params)
                graph.add_edge(other_core_index, core_index, **edge_params)

        return graph, edge_routers
