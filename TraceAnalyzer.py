#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json

from CriticalReroute import Cost, UpdateReport


class TraceAnalyzer:
    """
    Load a test case as described by the specification in TEST-FILE-FORMAT.md
    """

    def __init__(self, filename: str, skip_unweighted: bool):
        """
        Load the test file specified
        """
        self.test_filename = filename
        self.skip_unweighted = skip_unweighted

        with open(filename, 'r') as input:
            self.data = json.load(input)

    def toCSV(self) -> str:
        traces = [[UpdateReport(
            iteration=trace[0],
            elapsed_time=trace[1],
            new_cost=Cost(*trace[2]),
        ) for trace in tracelist] for tracelist in self.data['weighted-traces']]

        best_per_trace = map(lambda trace:                          # For each trace
                             min(trace,                             # Find the minimum
                                 key=lambda val: val.new_cost), traces)   # Compare on Cost, not iteration

        for trace in traces:
            print('overload', *[val.new_cost.overload_factor for val in trace], sep=',')
            print('elapsed_time', *[val.elapsed_time for val in trace], sep=',')
            print('iteration',*[val.iteration for val in trace], sep=',')
        pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Gather statistics for a test file")
    parser.add_argument("--filename", action='store', type=str, required=True,
                        help="Path to the test data file to read")
    parser.add_argument("--skip-unweighted", action='store', type=str, default=False,
                        help="Skip analysis and output of unweighted traces")

    args = parser.parse_args()

    analyzer = TraceAnalyzer(filename=args.filename, skip_unweighted=args.skip_unweighted)
    analyzer.toCSV()