#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json
from typing import List

class ResultCombiner:

    # Everything of this type is a list of values which will be combined into a single list of values (rather than lists of lists of values)
    COMBINEABLE_KEYS = [
        'weighted-costs',
        'weighted-traces',
        'weighted-midpoints',
        'unweighted-costs',
        'unweighted-traces',
        'unweighted-midpoints',
    ]

    # Everything in this list of a value, which will be combined as a list of values
    VALUE_KEYS = [
        'version',
        'num-repeats',
        'optimizer-version',
        'ospf-cost',
        'optimizer-params',
        'total-req-bandwidth',
        'date',
        'filename',
    ]

    SKIP_KEYS = [
        'traffic-matrix', # The traffic_matrix can be massive, and I don't care about it for my uses
    ]

    @staticmethod
    def combine(filenames: List[str]) -> str:
        data = {}

        for filename in filenames:
            with open(filename) as json_file:
                input = json.load(json_file)
                for key, value in input.items():
                    if key in ResultCombiner.COMBINEABLE_KEYS:
                        if not key in data:
                            data[key] = value
                        else:
                            data[key].extend(value)
                    elif key in ResultCombiner.VALUE_KEYS:
                        if not key in data:
                            data[key] = []
                        data[key].append(value)
                    elif key in ResultCombiner.SKIP_KEYS:
                        pass # Skip!
                    else:
                        raise ValueError("Unrecognized Key: {}".format(key))
                    pass

        return json.dumps(data)

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Combine the data from many test files into one")
    parser.add_argument("--filenames", nargs='+', action='store', type=str, required=True,
                        help="Path to the test data files to combine")

    args = parser.parse_args()

    print(ResultCombiner.combine(args.filenames))